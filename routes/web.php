<?php

use App\Http\Controllers\Billing\BillingController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\Customer\CustomerAddressController;
use App\Http\Controllers\Enterprise\EntContController;
use App\Http\Controllers\Enterprise\EnterpriseController;
use App\Http\Controllers\Invoice\InvoiceController;
use App\Http\Controllers\Order\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);


Route::middleware(['auth'])->group(function () {
    // Routes used from all authenticated users
    /* 
    NONE ATM 
    */
    // Routes used from all authenticated team users
    Route::middleware(['team'])->group(function () {
        Route::get('/', [HomeController::class, 'index'])->name('team.index');
        // Customers
        Route::get('/kunden', [CustomerController::class, 'index'])->name('team.customer.index');
        // create customer
        Route::get('/kunden/kunden-erfassen', [CustomerController::class, 'create'])->name('team.customer.create');
        Route::post('/kunden/kunden-erfassen', [CustomerController::class, 'store'])->name('team.customer.store');
        // create billing address
        Route::get('/kunden/kunden-erfassen/rechnungsadresse-hinzufuegen/{customer}', [CustomerAddressController::class, 'create'])->name('team.customer.billing-address.create');
        Route::post('/kunden/kunden-erfassen/rechnungsadresse-hinzufuegen/{customer}', [CustomerAddressController::class, 'store'])->name('team.customer.billing-address.store');
        // show customer
        Route::get('/kunden/{customer}', [CustomerController::class, 'show'])->name('team.customer.show');
        // invoices
        Route::get('/rechnungen', [InvoiceController::class, 'index'])->name('team.invoices.index');
        Route::get('/rechnungen/{invoice}', [InvoiceController::class, 'show'])->name('team.invoices.show');
        Route::get('/rechnungen/{invoice}/versenden', [InvoiceController::class, 'sendInitialInvoice'])->name('team.invoices.send.initial');
        Route::get('/rechnungen/{invoice}/send-to-user', [InvoiceController::class, 'approveInvoice'])->name('team.invoices.approve');
        // Enterprises
        Route::get('/firmen', [EnterpriseController::class, 'index'])->name('team.enterprise.index');
        Route::get('/firmen/firma-erfassen', [EnterpriseController::class, 'create'])->name('team.enterprise.create');
        Route::post('/firmen/firma-erfassen', [EnterpriseController::class, 'store'])->name('team.enterprise.store');
        Route::get('/firmen/{enterprise}', [EnterpriseController::class, 'show'])->name('team.enterprise.show');
        Route::get('/firmen/{enterprise}/kontakt-hinzufuegen', [EntContController::class, 'create'])->name('team.enterprise.contact.create');
        Route::post('/firmen/{enterprise}/kontakt-hinzufuegen', [EntContController::class, 'store'])->name('team.enterprise.contact.store');
        // Order
        Route::get('/auftragswesen', [OrderController::class, 'index'])->name('team.order.index');
        Route::get('/auftragswesen/auftrags-art', [OrderController::class, 'createType'])->name('team.order-type.create');
        Route::post('/auftragswesen/auftrags-art', [OrderController::class, 'storeType'])->name('team.order-type.store');
        Route::get('/auftragswesen/auftrag-erstellen', [OrderController::class, 'create'])->name('team.order.create');
        Route::post('/auftragswesen/auftrag-erstellen', [OrderController::class, 'init'])->name('team.order.init');
        Route::post('/auftragswesen/{order}/auftrag-aendern/{addOrRemove}', [OrderController::class, 'update'])->name('team.order.update');
        Route::get('/auftragswesen/{order}', [OrderController::class, 'show'])->name('team.order.show');
        Route::post('/auftragswesen/{order}/rechnungsadresse', [OrderController::class, 'billAdd'])->name('team.order.bill.add');
        Route::post('/auftragswesen/{order}/delete', [OrderController::class, 'delete'])->name('team.order.delete');
        // Billing
        Route::post('/auftragswesen/{order}/rechnung-erstellen', [BillingController::class, 'manualCreate'])->name('team.billing.create.manual');
    });
    // Routes used from all authenticated admin users
    Route::middleware(['admin'])->group(function () {
        /* 
        NONE ATM 
        */ });
});
