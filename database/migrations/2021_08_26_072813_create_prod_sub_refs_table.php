<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdSubRefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prod_sub_refs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prod_main_id');
            $table->unsignedBigInteger('prod_sub_id');
            $table->timestamps();
            
            $table->foreign('prod_main_id')->references('id')->on('prods')->onDelete('cascade');
            $table->foreign('prod_sub_id')->references('id')->on('prods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prod_sub_refs');
    }
}
