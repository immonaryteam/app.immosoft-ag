<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnterpriseLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_locations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ent_id');
            $table->unsignedBigInteger('loc_id');
            $table->boolean('is_billing')->default(0);
            $table->timestamps();

            $table->foreign('ent_id')->references('id')->on('enterprises')->onDelete('cascade');
            $table->foreign('loc_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_locations');
    }
}
