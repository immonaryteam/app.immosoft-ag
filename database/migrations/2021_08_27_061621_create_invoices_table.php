<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ord_id');
            $table->unsignedBigInteger('inv_status_code_id');
            $table->timestamps();

            $table->foreign('ord_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('inv_status_code_id')->references('id')->on('invoice_status_codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
