<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->integer('cus_account_id');
            $table->string('account_type');
            $table->boolean('is_active')->default(1);
            $table->string('salutation');
            $table->string('firstname');
            $table->string('lastname');
            $table->integer('mobile')->nullable();
            $table->integer('mobile_prefix')->default(0041);
            $table->integer('landline')->nullable();
            $table->integer('landline_prefix')->default(0041);
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
