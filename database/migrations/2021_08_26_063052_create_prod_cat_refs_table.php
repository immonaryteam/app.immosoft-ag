<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdCatRefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prod_cat_refs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prod_cat_id');
            $table->unsignedBigInteger('prod_id');
            $table->timestamps();

            $table->foreign('prod_cat_id')->references('id')->on('prod_cats')->onDelete('cascade');
            $table->foreign('prod_id')->references('id')->on('prods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prod_cat_refs');
    }
}
