<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cus_id'); // contact
            $table->unsignedBigInteger('billing_loc_id')->nullable(); // billing location id
            $table->integer('ord_nr');
            $table->decimal('ord_total')->default(0);
            $table->dateTime('ord_extern_date')->nullable();
            $table->dateTime('ord_intern_date')->nullable();
            $table->timestamps();
            // foreign keys binding
            $table->foreign('cus_id')->references('id')->on('customers');
            $table->foreign('billing_loc_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
