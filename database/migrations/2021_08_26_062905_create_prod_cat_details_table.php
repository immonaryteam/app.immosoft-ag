<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdCatDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prod_cat_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prod_cat_id');
            $table->string('prod_cat_name');
            $table->boolean('prod_cat_has_price')->default(0);
            $table->decimal('prod_cat_price')->nullable();
            $table->timestamps();

            $table->foreign('prod_cat_id')->references('id')->on('prod_cats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prod_cat_details');
    }
}
