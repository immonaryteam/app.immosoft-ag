<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ord_id');
            $table->unsignedBigInteger('prod_id');
            $table->string('ord_item_desc');
            $table->integer('ord_item_quantity');
            $table->string('ord_item_quantity_type');
            $table->decimal('ord_item_price');
            $table->timestamps();

            $table->foreign('ord_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('prod_id')->references('id')->on('prods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
