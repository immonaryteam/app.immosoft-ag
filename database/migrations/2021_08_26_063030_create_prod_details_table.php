<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prod_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prod_id');
            $table->string('prod_name');
            $table->boolean('prod_has_price')->default(0);
            $table->decimal('prod_price')->nullable();
            $table->boolean('prod_is_sub')->default(0);
            $table->timestamps();
            
            $table->foreign('prod_id')->references('id')->on('prods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prod_details');
    }
}
