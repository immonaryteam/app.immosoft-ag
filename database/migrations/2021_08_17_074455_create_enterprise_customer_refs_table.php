<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnterpriseCustomerRefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_customer_refs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ent_id');
            $table->unsignedBigInteger('cus_id');
            $table->timestamps();

            $table->foreign('ent_id')->references('id')->on('enterprises');
            $table->foreign('cus_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_customer_refs');
    }
}
