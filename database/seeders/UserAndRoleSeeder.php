<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserAndRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //set roles
        DB::table('roles')->insert([
            'id' => 1,
            'role_name' => 'admin'
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'role_name' => 'team'
        ]);
        //set user for testing
        DB::table('users')->insert([
            'id' => 1,
            'role_id' => 2,
            'salutation' => 'male',
            'firstname' => 'Manuel',
            'lastname' => 'Steiner',
            'email' => 'm.steiner@immosoft-ag.ch',
            'password' => bcrypt('123456'),
        ]);
    }
}
