<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeds for prod_cats_table
        DB::table('prod_cats')->insert([
            'id' => 1,
            'prod_cat_art_nr' => 10,
        ]);
        DB::table('prod_cats')->insert([
            'id' => 2,
            'prod_cat_art_nr' => 20,
        ]);
        DB::table('prod_cats')->insert([
            'id' => 3,
            'prod_cat_art_nr' => 30,
        ]);
        DB::table('prod_cats')->insert([
            'id' => 4,
            'prod_cat_art_nr' => 40,
        ]);
        DB::table('prod_cats')->insert([
            'id' => 5,
            'prod_cat_art_nr' => 50,
        ]);
        DB::table('prod_cats')->insert([
            'id' => 6,
            'prod_cat_art_nr' => 60,
        ]);
        DB::table('prod_cats')->insert([
            'id' => 7,
            'prod_cat_art_nr' => 70,
        ]);
        DB::table('prod_cats')->insert([
            'id' => 8,
            'prod_cat_art_nr' => 80,
        ]);
        DB::table('prod_cats')->insert([
            'id' => 9,
            'prod_cat_art_nr' => 90,
        ]);
        DB::table('prod_cats')->insert([
            'id' => 10,
            'prod_cat_art_nr' => 100,
        ]);
        // add prod_cat_details
        DB::table('prod_cat_details')->insert([
            'id' => 1,
            'prod_cat_id' => 1,
            'prod_cat_name' => 'Verkaufsdokumentation',
            'prod_cat_has_price' => 1,
            'prod_cat_price' => 1950
        ]);
        DB::table('prod_cat_details')->insert([
            'id' => 2,
            'prod_cat_id' => 2,
            'prod_cat_name' => 'Projekt Homepage',
            'prod_cat_has_price' => 1,
            'prod_cat_price' => 2600
        ]);
        DB::table('prod_cat_details')->insert([
            'id' => 3,
            'prod_cat_id' => 3,
            'prod_cat_name' => 'Bautafel / Plache',
            'prod_cat_has_price' => 1,
            'prod_cat_price' => 650
        ]);
        DB::table('prod_cat_details')->insert([
            'id' => 4,
            'prod_cat_id' => 4,
            'prod_cat_name' => 'Aufbereitung und / oder erstellen der Grunddaten',
            'prod_cat_has_price' => 0,
            'prod_cat_price' => null
        ]);
        DB::table('prod_cat_details')->insert([
            'id' => 5,
            'prod_cat_id' => 5,
            'prod_cat_name' => 'Liegenschaftsfotos',
            'prod_cat_has_price' => 0,
            'prod_cat_price' => null
        ]);
        DB::table('prod_cat_details')->insert([
            'id' => 6,
            'prod_cat_id' => 6,
            'prod_cat_name' => 'Visualisierungen',
            'prod_cat_has_price' => 0,
            'prod_cat_price' => null
        ]);
        DB::table('prod_cat_details')->insert([
            'id' => 7,
            'prod_cat_id' => 7,
            'prod_cat_name' => '360° Rundgang',
            'prod_cat_has_price' => 0,
            'prod_cat_price' => null
        ]);
        DB::table('prod_cat_details')->insert([
            'id' => 8,
            'prod_cat_id' => 8,
            'prod_cat_name' => 'Online-Marketing',
            'prod_cat_has_price' => 0,
            'prod_cat_price' => null
        ]);
        DB::table('prod_cat_details')->insert([
            'id' => 9,
            'prod_cat_id' => 9,
            'prod_cat_name' => 'Kombipaket für mittlere Objekte',
            'prod_cat_has_price' => 1,
            'prod_cat_price' => 12500
        ]);
        DB::table('prod_cat_details')->insert([
            'id' => 10,
            'prod_cat_id' => 10,
            'prod_cat_name' => 'Individueller Fachsupport',
            'prod_cat_has_price' => 0,
            'prod_cat_price' => NULL
        ]);




        // seeds for products_table

        //prodCat10
        DB::table('prods')->insert([
            'id' => 1,
            'prod_art_nr' => 11,
        ]);
        DB::table('prods')->insert([
            'id' => 2,
            'prod_art_nr' => 12,
        ]);
        DB::table('prods')->insert([
            'id' => 3,
            'prod_art_nr' => 13,
        ]);
        DB::table('prods')->insert([
            'id' => 4,
            'prod_art_nr' => 14,
        ]);
        //prodCat20
        DB::table('prods')->insert([
            'id' => 5,
            'prod_art_nr' => 21,
        ]);
        DB::table('prods')->insert([
            'id' => 6,
            'prod_art_nr' => 22,
        ]);
        DB::table('prods')->insert([
            'id' => 7,
            'prod_art_nr' => 23,
        ]);
        DB::table('prods')->insert([
            'id' => 8,
            'prod_art_nr' => 24,
        ]);
        DB::table('prods')->insert([
            'id' => 9,
            'prod_art_nr' => 24.1,
        ]);
        DB::table('prods')->insert([
            'id' => 10,
            'prod_art_nr' => 24.2,
        ]);
        DB::table('prods')->insert([
            'id' => 11,
            'prod_art_nr' => 24.3,
        ]);
        DB::table('prods')->insert([
            'id' => 12,
            'prod_art_nr' => 24.4,
        ]);
        //prodCat30
        DB::table('prods')->insert([
            'id' => 13,
            'prod_art_nr' => 31,
        ]);
        DB::table('prods')->insert([
            'id' => 14,
            'prod_art_nr' => 32,
        ]);
        DB::table('prods')->insert([
            'id' => 15,
            'prod_art_nr' => 33,
        ]);
        //prodCat40
        DB::table('prods')->insert([
            'id' => 16,
            'prod_art_nr' => 41,
        ]);
        DB::table('prods')->insert([
            'id' => 17,
            'prod_art_nr' => 42,
        ]);
        DB::table('prods')->insert([
            'id' => 18,
            'prod_art_nr' => 42.1,
        ]);
        DB::table('prods')->insert([
            'id' => 19,
            'prod_art_nr' => 42.2,
        ]);
        DB::table('prods')->insert([
            'id' => 20,
            'prod_art_nr' => 42.3,
        ]);
        DB::table('prods')->insert([
            'id' => 21,
            'prod_art_nr' => 43,
        ]);
        DB::table('prods')->insert([
            'id' => 22,
            'prod_art_nr' => 44,
        ]);
        //prodCat50
        DB::table('prods')->insert([
            'id' => 23,
            'prod_art_nr' => 51,
        ]);
        DB::table('prods')->insert([
            'id' => 24,
            'prod_art_nr' => 52,
        ]);
        //prodCat60
        DB::table('prods')->insert([
            'id' => 25,
            'prod_art_nr' => 61,
        ]);
        DB::table('prods')->insert([
            'id' => 26,
            'prod_art_nr' => 62,
        ]);
        DB::table('prods')->insert([
            'id' => 27,
            'prod_art_nr' => 63,
        ]);
        //prodCat70
        DB::table('prods')->insert([
            'id' => 28,
            'prod_art_nr' => 71,
        ]);
        DB::table('prods')->insert([
            'id' => 29,
            'prod_art_nr' => 72,
        ]);
        DB::table('prods')->insert([
            'id' => 30,
            'prod_art_nr' => 73,
        ]);
        DB::table('prods')->insert([
            'id' => 31,
            'prod_art_nr' => 73.1,
        ]);
        DB::table('prods')->insert([
            'id' => 32,
            'prod_art_nr' => 73.2,
        ]);
        DB::table('prods')->insert([
            'id' => 33,
            'prod_art_nr' => 73.3,
        ]);
        DB::table('prods')->insert([
            'id' => 34,
            'prod_art_nr' => 73.4,
        ]);
        DB::table('prods')->insert([
            'id' => 35,
            'prod_art_nr' => 73.5,
        ]);
        DB::table('prods')->insert([
            'id' => 36,
            'prod_art_nr' => 74,
        ]);
        DB::table('prods')->insert([
            'id' => 37,
            'prod_art_nr' => 74.1,
        ]);
        DB::table('prods')->insert([
            'id' => 38,
            'prod_art_nr' => 74.2,

        ]);
        DB::table('prods')->insert([
            'id' => 39,
            'prod_art_nr' => 74.3,

        ]);
        DB::table('prods')->insert([
            'id' => 40,
            'prod_art_nr' => 74.4,

        ]);
        DB::table('prods')->insert([
            'id' => 41,
            'prod_art_nr' => 74.5,

        ]);
        DB::table('prods')->insert([
            'id' => 42,
            'prod_art_nr' => 74.6,

        ]);
        //prodCat80
        DB::table('prods')->insert([
            'id' => 43,
            'prod_art_nr' => 81,

        ]);
        DB::table('prods')->insert([
            'id' => 44,
            'prod_art_nr' => 81.1,
        ]);
        DB::table('prods')->insert([
            'id' => 45,
            'prod_art_nr' => 81.2,
        ]);
        DB::table('prods')->insert([
            'id' => 46,
            'prod_art_nr' => 81.3,
        ]);
        DB::table('prods')->insert([
            'id' => 47,
            'prod_art_nr' => 82,
        ]);
        DB::table('prods')->insert([
            'id' => 48,
            'prod_art_nr' => 82.1,

        ]);
        DB::table('prods')->insert([
            'id' => 49,
            'prod_art_nr' => 82.2,

        ]);
        //prodCat10
        DB::table('prods')->insert([
            'id' => 50,
            'prod_art_nr' => 91,

        ]);
        DB::table('prods')->insert([
            'id' => 51,
            'prod_art_nr' => 92,

        ]);
        DB::table('prods')->insert([
            'id' => 52,
            'prod_art_nr' => 93,

        ]);
        DB::table('prods')->insert([
            'id' => 53,
            'prod_art_nr' => 94,

        ]);
        DB::table('prods')->insert([
            'id' => 54,
            'prod_art_nr' => 95,

        ]);
        DB::table('prods')->insert([
            'id' => 55,
            'prod_art_nr' => 96,

        ]);
        DB::table('prods')->insert([
            'id' => 56,
            'prod_art_nr' => 97,

        ]);
        DB::table('prods')->insert([
            'id' => 57,
            'prod_art_nr' => 101,

        ]);
        //prod details
        DB::table('prod_details')->insert([
            'id' => 1,
            'prod_id' => 1,
            'prod_name' => 'Konzept CI und CD',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 2,
            'prod_id' => 2,
            'prod_name' => 'Design / Layout für 12 Seiten A4 Format',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 3,
            'prod_id' => 3,
            'prod_name' => 'Umgebungsaufnahmen inkl. Bildbearbeitung',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 4,
            'prod_id' => 4,
            'prod_name' => 'Textvorlage inkl. Makrolagetext',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        //prodCat20
        DB::table('prod_details')->insert([
            'id' => 5,
            'prod_id' => 5,
            'prod_name' => 'Konzept (Struktur, Menü Punkte)',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 6,
            'prod_id' => 6,
            'prod_name' => 'Layout / Design (nach CI/CD Vorgabe)',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 7,
            'prod_id' => 7,
            'prod_name' => 'CMS zur Selbstverwaltung für Vermietungsstand',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 8,
            'prod_id' => 8,
            'prod_name' => 'Web-Programmierung',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 9,
            'prod_id' => 9,
            'prod_name' => 'Verwendete Programmiersprachen: HTML5, CSS3, JS/jQuery, PHP, MySQL',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 10,
            'prod_id' => 10,
            'prod_name' => 'Responsive Design',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 11,
            'prod_id' => 11,
            'prod_name' => 'Google Map & Kontaktformular',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 12,
            'prod_id' => 12,
            'prod_name' => 'Suchmaschinenoptimierung (SEO Onpage & Offpage)',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 1,
        ]);
        //prodCat30
        DB::table('prod_details')->insert([
            'id' => 13,
            'prod_id' => 13,
            'prod_name' => 'Layout / Design (nach CI/CD Verkaufsdokumentation)',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 14,
            'prod_id' => 14,
            'prod_name' => 'Format frei wählbar',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 15,
            'prod_id' => 15,
            'prod_name' => 'Koordination / Druckvorstufe',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        //prodCat40
        DB::table('prod_details')->insert([
            'id' => 16,
            'prod_id' => 16,
            'prod_name' => 'Logo',
            'prod_has_price' => 1,
            'prod_price' => 450,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 17,
            'prod_id' => 17,
            'prod_name' => 'Grundrisse',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 18,
            'prod_id' => 18,
            'prod_name' => 'Grundrissaufbereitung',
            'prod_has_price' => 1,
            'prod_price' => 100,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 19,
            'prod_id' => 19,
            'prod_name' => 'Grundriss neu erstellt ab Vorlage',
            'prod_has_price' => 1,
            'prod_price' => 150,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 20,
            'prod_id' => 20,
            'prod_name' => 'Grundriss komplett neu erstellt, inkl. Vermessung und Aufbereitung',
            'prod_has_price' => 1,
            'prod_price' => 600,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 21,
            'prod_id' => 21,
            'prod_name' => 'Aufbereitung Situationsplan aus jeweiligem GIS-Browser',
            'prod_has_price' => 1,
            'prod_price' => 300,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 22,
            'prod_id' => 22,
            'prod_name' => 'Erstellung Lageplan',
            'prod_has_price' => 1,
            'prod_price' => 300,
            'prod_is_sub' => 0,
        ]);
        //prodCat50
        DB::table('prod_details')->insert([
            'id' => 23,
            'prod_id' => 23,
            'prod_name' => 'Professionelle Fotoaufnahmen inkl. Bearbeitung, 6 - 8 Aufnahmen',
            'prod_has_price' => 1,
            'prod_price' => 450,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 24,
            'prod_id' => 24,
            'prod_name' => 'Bearbeitung respektive Aufarbeitung einer bestehender Fotoaufnahme (je nach Aufwand weicht dieser Preis ab)',
            'prod_has_price' => 1,
            'prod_price' => 80,
            'prod_is_sub' => 0,
        ]);
        //prodCat60
        DB::table('prod_details')->insert([
            'id' => 25,
            'prod_id' => 25,
            'prod_name' => 'Innenvisualisierungen',
            'prod_has_price' => 1,
            'prod_price' => 2000,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 26,
            'prod_id' => 26,
            'prod_name' => 'Aussenvisualisierungen',
            'prod_has_price' => 1,
            'prod_price' => 2500,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 27,
            'prod_id' => 27,
            'prod_name' => 'Drohnenaufnahmen mit hochauflésender Optik mit 6 - 10 Standpunkten',
            'prod_has_price' => 1,
            'prod_price' => 600,
            'prod_is_sub' => 0,
        ]);
        //prodCat70
        DB::table('prod_details')->insert([
            'id' => 28,
            'prod_id' => 28,
            'prod_name' => 'Standard Rundgang mit 6 - 10 Standpunkten',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 29,
            'prod_id' => 29,
            'prod_name' => 'Premium Rundgang mit hochauflösender Optik mit 6 - 10 Standpunkten',
            'prod_has_price' => 1,
            'prod_price' => 800,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 30,
            'prod_id' => 30,
            'prod_name' => 'Weitere Dienstleistungen pro Standpunkt',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 31,
            'prod_id' => 31,
            'prod_name' => 'Videosequenzen (geliefertes Video)',
            'prod_has_price' => 1,
            'prod_price' => 15,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 32,
            'prod_id' => 32,
            'prod_name' => 'Informationstext pro Standpunkt / pro Text',
            'prod_has_price' => 1,
            'prod_price' => 20,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 33,
            'prod_id' => 33,
            'prod_name' => 'Symbole / Effekte',
            'prod_has_price' => 1,
            'prod_price' => 20,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 34,
            'prod_id' => 34,
            'prod_name' => 'Anonymisierungsmöglichkeiten',
            'prod_has_price' => 1,
            'prod_price' => 25,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 35,
            'prod_id' => 35,
            'prod_name' => 'Verlinkungen von Objekten mit externer Homepage pro Verlinkung',
            'prod_has_price' => 1,
            'prod_price' => 15,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 36,
            'prod_id' => 36,
            'prod_name' => 'Weitere Dienstleistungen gesamte Tour',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 37,
            'prod_id' => 37,
            'prod_name' => 'Logo',
            'prod_has_price' => 1,
            'prod_price' => 20,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 38,
            'prod_id' => 38,
            'prod_name' => 'Grundriss',
            'prod_has_price' => 1,
            'prod_price' => 20,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 39,
            'prod_id' => 39,
            'prod_name' => 'Kompass',
            'prod_has_price' => 1,
            'prod_price' => 25,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 40,
            'prod_id' => 40,
            'prod_name' => 'Wetterauswahl draussen (Regen / Schnee)',
            'prod_has_price' => 1,
            'prod_price' => 20,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 41,
            'prod_id' => 41,
            'prod_name' => 'Verschlüsselter Rundgang mit Passwort',
            'prod_has_price' => 1,
            'prod_price' => 50,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 42,
            'prod_id' => 42,
            'prod_name' => 'jede weitere Standpunkt',
            'prod_has_price' => 1,
            'prod_price' => 2.5,
            'prod_is_sub' => 1,
        ]);
        //prodCat80
        DB::table('prod_details')->insert([
            'id' => 43,
            'prod_id' => 43,
            'prod_name' => 'Facebook',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 44,
            'prod_id' => 44,
            'prod_name' => 'Erstellung einer Facebook-Fanpage',
            'prod_has_price' => 1,
            'prod_price' => 350,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 45,
            'prod_id' => 45,
            'prod_name' => 'Betreuung einer Facebook-Fanpage pro Monat bei 2 bis 4 Ergänzungen',
            'prod_has_price' => 1,
            'prod_price' => 320,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 46,
            'prod_id' => 46,
            'prod_name' => 'Facebookwerbung je nach Marketingbudget',
            'prod_has_price' => 1,
            'prod_price' => 500,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 47,
            'prod_id' => 47,
            'prod_name' => 'Google',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 48,
            'prod_id' => 48,
            'prod_name' => 'Google Ads Initialisierung',
            'prod_has_price' => 1,
            'prod_price' => 1000,
            'prod_is_sub' => 1,
        ]);
        DB::table('prod_details')->insert([
            'id' => 49,
            'prod_id' => 49,
            'prod_name' => 'Google Ads Betreuung eines registrierten Kunden',
            'prod_has_price' => 1,
            'prod_price' => 500,
            'prod_is_sub' => 0,
        ]);
        //prodCat10
        DB::table('prod_details')->insert([
            'id' => 50,
            'prod_id' => 50,
            'prod_name' => '1 Logo (3 Entwürfe)',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 51,
            'prod_id' => 51,
            'prod_name' => '3 Visualisierungen (2x innen, 1x aussen)',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 52,
            'prod_id' => 52,
            'prod_name' => 'Broschüre, Format frei wählbar (12 -20 Seiten)',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 53,
            'prod_id' => 53,
            'prod_name' => 'Projekthomepage',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 54,
            'prod_id' => 54,
            'prod_name' => '1x Plache / Bautafel (Mass frei wählbar) exkl. Druck',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 55,
            'prod_id' => 55,
            'prod_name' => 'Komplette Datenaufbereitung (Grundrisse: maximal 8 Stück)',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 56,
            'prod_id' => 56,
            'prod_name' => 'Fotoaufnahme vor Ort',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,
        ]);
        DB::table('prod_details')->insert([
            'id' => 57,
            'prod_id' => 57,
            'prod_name' => 'Individueller Fachsupport',
            'prod_has_price' => 0,
            'prod_price' => null,
            'prod_is_sub' => 0,

        ]);
        //prod_cat_reference
        DB::table('prod_cat_refs')->insert([
            'id' => 1,
            'prod_cat_id' => 1,
            'prod_id' => 1,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 2,
            'prod_cat_id' => 1,
            'prod_id' => 2,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 3,
            'prod_cat_id' => 1,
            'prod_id' => 3,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 4,
            'prod_cat_id' => 1,
            'prod_id' => 4,
        ]);
        //prodCat20
        DB::table('prod_cat_refs')->insert([
            'id' => 5,
            'prod_cat_id' => 2,
            'prod_id' => 5,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 6,
            'prod_cat_id' => 2,
            'prod_id' => 6,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 7,
            'prod_cat_id' => 2,
            'prod_id' => 7,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 8,
            'prod_cat_id' => 2,
            'prod_id' => 8,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 9,
            'prod_cat_id' => 2,
            'prod_id' => 9,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 10,
            'prod_cat_id' => 2,
            'prod_id' => 10,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 11,
            'prod_cat_id' => 2,
            'prod_id' => 11,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 12,
            'prod_cat_id' => 2,
            'prod_id' => 12,
        ]);
        //prodCat30
        DB::table('prod_cat_refs')->insert([
            'id' => 13,
            'prod_cat_id' => 3,
            'prod_id' => 13,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 14,
            'prod_cat_id' => 3,
            'prod_id' => 14,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 15,
            'prod_cat_id' => 3,
            'prod_id' => 15,
        ]);
        //prodCat40
        DB::table('prod_cat_refs')->insert([
            'id' => 16,
            'prod_cat_id' => 4,
            'prod_id' => 16,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 17,
            'prod_cat_id' => 4,
            'prod_id' => 17,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 18,
            'prod_cat_id' => 4,
            'prod_id' => 18,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 19,
            'prod_cat_id' => 4,
            'prod_id' => 19,

        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 20,
            'prod_cat_id' => 4,
            'prod_id' => 20,

        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 21,
            'prod_cat_id' => 4,
            'prod_id' => 21,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 22,
            'prod_cat_id' => 4,
            'prod_id' => 22,

        ]);
        //prodCat50
        DB::table('prod_cat_refs')->insert([
            'id' => 23,
            'prod_cat_id' => 5,
            'prod_id' => 23,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 24,
            'prod_cat_id' => 5,
            'prod_id' => 24,
        ]);
        //prodCat60
        DB::table('prod_cat_refs')->insert([
            'id' => 25,
            'prod_cat_id' => 6,
            'prod_id' => 25,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 26,
            'prod_cat_id' => 6,
            'prod_id' => 26,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 27,
            'prod_cat_id' => 6,
            'prod_id' => 27,
        ]);
        //prodCat70
        DB::table('prod_cat_refs')->insert([
            'id' => 28,
            'prod_cat_id' => 7,
            'prod_id' => 28,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 29,
            'prod_cat_id' => 7,
            'prod_id' => 29,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 30,
            'prod_cat_id' => 7,
            'prod_id' => 30,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 31,
            'prod_cat_id' => 7,
            'prod_id' => 31,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 32,
            'prod_cat_id' => 7,
            'prod_id' => 32,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 33,
            'prod_cat_id' => 7,
            'prod_id' => 33,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 34,
            'prod_cat_id' => 7,
            'prod_id' => 34,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 35,
            'prod_cat_id' => 7,
            'prod_id' => 35,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 36,
            'prod_cat_id' => 7,
            'prod_id' => 36,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 37,
            'prod_cat_id' => 7,
            'prod_id' => 37,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 38,
            'prod_cat_id' => 7,
            'prod_id' => 38,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 39,
            'prod_cat_id' => 7,
            'prod_id' => 39,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 40,
            'prod_cat_id' => 7,
            'prod_id' => 40,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 41,
            'prod_cat_id' => 7,
            'prod_id' => 41,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 42,
            'prod_cat_id' => 7,
            'prod_id' => 42,
        ]);
        //prodCat80
        DB::table('prod_cat_refs')->insert([
            'id' => 43,
            'prod_cat_id' => 8,
            'prod_id' => 43,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 44,
            'prod_cat_id' => 8,
            'prod_id' => 44,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 45,
            'prod_cat_id' => 8,
            'prod_id' => 45,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 46,
            'prod_cat_id' => 8,
            'prod_id' => 46,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 47,
            'prod_cat_id' => 8,
            'prod_id' => 47,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 48,
            'prod_cat_id' => 8,
            'prod_id' => 48,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 49,
            'prod_cat_id' => 8,
            'prod_id' => 49,
        ]);
        //prodCat10
        DB::table('prod_cat_refs')->insert([
            'id' => 50,
            'prod_cat_id' => 9,
            'prod_id' => 50,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 51,
            'prod_cat_id' => 9,
            'prod_id' => 51,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 52,
            'prod_cat_id' => 9,
            'prod_id' => 52,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 53,
            'prod_cat_id' => 9,
            'prod_id' => 53,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 54,
            'prod_cat_id' => 9,
            'prod_id' => 54,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 55,
            'prod_cat_id' => 9,
            'prod_id' => 55,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 56,
            'prod_cat_id' => 9,
            'prod_id' => 56,
        ]);
        DB::table('prod_cat_refs')->insert([
            'id' => 57,
            'prod_cat_id' => 10,
            'prod_id' => 57,

        ]);
        //next
        DB::table('prod_sub_refs')->insert([
            'id' => 1,
            'prod_main_id' => 8,
            'prod_sub_id' => 9,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 2,
            'prod_main_id' => 8,
            'prod_sub_id' => 10,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 3,
            'prod_main_id' => 8,
            'prod_sub_id' => 11,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 4,
            'prod_main_id' => 8,
            'prod_sub_id' => 12,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 5,
            'prod_main_id' => 17,
            'prod_sub_id' => 18,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 6,
            'prod_main_id' => 17,
            'prod_sub_id' => 19,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 7,
            'prod_main_id' => 17,
            'prod_sub_id' => 20,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 8,
            'prod_main_id' => 30,
            'prod_sub_id' => 31,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 9,
            'prod_main_id' => 30,
            'prod_sub_id' => 32,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 10,
            'prod_main_id' => 30,
            'prod_sub_id' => 33,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 11,
            'prod_main_id' => 30,
            'prod_sub_id' => 34,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 12,
            'prod_main_id' => 30,
            'prod_sub_id' => 35,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 13,
            'prod_main_id' => 36,
            'prod_sub_id' => 37,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 14,
            'prod_main_id' => 36,
            'prod_sub_id' => 38,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 15,
            'prod_main_id' => 36,
            'prod_sub_id' => 39,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 16,
            'prod_main_id' => 36,
            'prod_sub_id' => 40,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 17,
            'prod_main_id' => 36,
            'prod_sub_id' => 41,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 18,
            'prod_main_id' => 36,
            'prod_sub_id' => 42,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 19,
            'prod_main_id' => 43,
            'prod_sub_id' => 44,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 20,
            'prod_main_id' => 43,
            'prod_sub_id' => 45,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 21,
            'prod_main_id' => 43,
            'prod_sub_id' => 46,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 22,
            'prod_main_id' => 47,
            'prod_sub_id' => 48,
        ]);
        DB::table('prod_sub_refs')->insert([
            'id' => 23,
            'prod_main_id' => 47,
            'prod_sub_id' => 49,
        ]);
    }
}
