<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // invoice status codes
        DB::table('invoice_status_codes')->insert([
            'id' => 1,
            'inv_status_desc' => 'Rechnungsstellung (Überprüfung)'
        ]);
        DB::table('invoice_status_codes')->insert([
            'id' => 2,
            'inv_status_desc' => '1. Mahnung'
        ]);
        DB::table('invoice_status_codes')->insert([
            'id' => 3,
            'inv_status_desc' => '2. Mahnung'
        ]);
        DB::table('invoice_status_codes')->insert([
            'id' => 4,
            'inv_status_desc' => '3. Mahnung'
        ]);
        DB::table('invoice_status_codes')->insert([
            'id' => 5,
            'inv_status_desc' => 'Betreibung'
        ]);
        DB::table('invoice_status_codes')->insert([
            'id' => 6,
            'inv_status_desc' => 'on Hold'
        ]);
        DB::table('invoice_status_codes')->insert([
            'id' => 7,
            'inv_status_desc' => 'Bezahlt'
        ]);
        DB::table('invoice_status_codes')->insert([
            'id' => 8,
            'inv_status_desc' => 'In Rechnung gestellt'
        ]);
        DB::table('invoice_status_codes')->insert([
            'id' => 9,
            'inv_status_desc' => 'Rechnung storniert'
        ]);
        // order status codes
        DB::table('order_status_codes')->insert([
            'id' => 1,
            'ord_status_desc' => 'Auftrag initialisiert'
        ]);
        DB::table('order_status_codes')->insert([
            'id' => 2,
            'ord_status_desc' => 'In Bearbeitung'
        ]);
        DB::table('order_status_codes')->insert([
            'id' => 3,
            'ord_status_desc' => 'On Hold'
        ]);
        DB::table('order_status_codes')->insert([
            'id' => 4,
            'ord_status_desc' => 'Rechnungsstellung'
        ]);
        DB::table('order_status_codes')->insert([
            'id' => 5,
            'ord_status_desc' => 'Abgeschlossen'
        ]);
        DB::table('order_status_codes')->insert([
            'id' => 6,
            'ord_status_desc' => 'Storniert'
        ]);
    }
}
