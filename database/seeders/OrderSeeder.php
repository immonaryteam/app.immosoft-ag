<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_types')->insert([
            'id' => 1,
            'ord_type_desc' => 'Einzelauftrag'
        ]);
        DB::table('order_types')->insert([
            'id' => 2,
            'ord_type_desc' => 'Wiederkehrender Auftrag'
        ]);
    }
}
