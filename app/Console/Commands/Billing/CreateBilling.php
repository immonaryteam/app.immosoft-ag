<?php

namespace App\Console\Commands\Billing;

use PDF;
use App\Models\Order\Order;
use Illuminate\Support\Carbon;
use App\Models\Invoice\Invoice;
use App\Models\Order\OrderItem;
use Illuminate\Console\Command;
use App\Models\Customer\Customer;
use App\Models\Location\Location;
use App\Models\Enterprise\Enterprise;
use Illuminate\Support\Facades\Storage;
use App\Services\Custom\Order\OrderService;
use App\Models\Enterprise\EnterpriseLocation;
use App\Services\Custom\General\MonthService;
use App\Services\Custom\Invoice\InvNrService;
use App\Models\Enterprise\EnterpriseCustomerRef;

class CreateBilling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command starts creating the billing process';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(InvNrService $invNrSrvc, MonthService $monthService, OrderService $orderService)
    {
        $lastMonth = Carbon::now()->subMonth(1)->month;
     
        $orders = Order::whereBetween('ord_status_code_id', [1, 3])->whereMonth('created_at', $lastMonth)->get();

        foreach ($orders as $ord) {
            $ordID = $ord->id;
            // invoice nr service
            $invNr = $invNrSrvc->createInvNr();
            //TODO: check if invoice exists
            $inv = new Invoice;
            $inv->inv_nr = $invNr;
            $inv->ord_id = $ordID;
            $inv->inv_status_code_id = 1;
            $inv->save();
            $ord->ord_status_code_id = 4;
            $ord->save();
            // get the commending customer ID
            $cusID = $ord->cus_id;
            $cus = Customer::find($cusID);
            // check if customer is business
            $entRefCheck = EnterpriseCustomerRef::where('cus_id', $cusID)->first();
            if (empty($entRefCheck)) {
                $check = 0;
                $cus = $cus;
                $ent = NULL;
                /* TODO: Invoice adress private */
                $loc = NULL;
            } else {
                $check = 1;
                $cus = NULL;
                $entID = $entRefCheck->ent_id;
                $ent = Enterprise::find($entID);
                $entLoc = EnterpriseLocation::where(['ent_id' =>  $entID, 'is_billing' => 1])->first();
                $entLocID = $entLoc->loc_id;
                $loc = Location::find($entLocID);
            }
            $specItemCheck = OrderItem::where(['ord_id' => $ordID, 'prod_id' => 57])->first();
            $portalItemCheck = OrderItem::where(['ord_id' => $ordID, 'prod_id' => 58])->first();
            $otherItemCheck = OrderItem::where('ord_id', $ordID)->where('prod_id', '!=', 57)->first();

            $specPriceTot = 0;
            $portalPriceTot = 0;

            $specItems = OrderItem::where(['ord_id' => $ordID, 'prod_id' => 57])->get();
            $portalItems = OrderItem::where(['ord_id' => $ordID, 'prod_id' => 58])->get();
            $otherItems = OrderItem::where('ord_id', $ordID)->where('prod_id', '!=', 57)->get();

            $totalItems = OrderItem::where('ord_id', $ordID)->get();


            $priceTot = 0;
            foreach ($totalItems as $totalItem) {
                $singlePrice = $totalItem->ord_item_price;
                if ($totalItem->ord_item_quantity_type == "min") {
                    $itemPrice = ($singlePrice / 60) * $totalItem->ord_item_quantity;;
                } elseif ($totalItem->ord_item_quantity_type = "pce") {
                    $itemPrice = $singlePrice * $totalItem->ord_item_quantity;
                }
                $priceTot += $itemPrice;
            }
            $mwst = ($priceTot / 100) * 7.7;
            $mwst = round($mwst / .05, 0) * .05;
            /* dd($mwst); */

            $priceTot = $priceTot + $mwst;

            if (!empty($specItemCheck)) {
                foreach ($specItems as $specItem) {
                    $price = $specItem->ord_item_price;
                    $quantity = $specItem->ord_item_quantity;
                    $type = $specItem->ord_item_quantity_type;

                    if ($type == "min") {
                        $tempPrice = ($price / 60) * $quantity;
                    } elseif ($type == "pce" || $type == "day") {
                        $tempPrice = $price * $quantity;
                    }

                    $specPriceTot += $tempPrice;
                }
            }
            $specItemArtNr = 101;
            /* Portal */
            if (!empty($portalItemCheck)) {
                foreach ($portalItems as $portalItem) {
                    $price = $portalItem->ord_item_price;
                    $quantity = $portalItem->ord_item_quantity;
                    $type = $portalItem->ord_item_quantity_type;

                    if ($type == "min") {
                        $tempPrice = ($price / 60) * $quantity;
                    } elseif ($type == "pce" || $type == "day") {
                        $tempPrice = $price * $quantity;
                    }
                    $portalPriceTot += $tempPrice;
                }
            }
            $portalItemArtNr = 111;

            $now = Carbon::now()->toDateTimeString();
            $nowMt = Date('m', strtotime($now));
            $month = $monthService->getGermanMonth($nowMt);
            $day = Date('d', strtotime($now));
            $year = Date('Y', strtotime($now));


            ini_set('memory_limit', '2048M');
            $pdf = PDF::loadView('pdf.invoice', compact(
                'inv',
                'cus',
                'ent',
                'check',
                'loc',
                'specItemCheck',
                'specItemArtNr',
                'specPriceTot',
                'portalItemCheck',
                'portalItemArtNr',
                'portalPriceTot',
                'otherItems',
                'otherItemCheck',
                'ord',
                'month',
                'day',
                'mwst',
                'priceTot',
                'specItems',
                'portalItems'
            ));
            $contents = $pdf->download()->getOriginalContent();

            if ($ord->ord_type_id == 2) {
                $newOrd = new Order;
                $newOrd->ord_status_code_id = 1;
                $newOrd->ord_type_id = 2;
                $newOrd->recc_freq = $ord->recc_freq;
                $newOrd->cus_id = $ord->cus_id;
                $newOrd->billing_loc_id = $ord->billing_loc_id;
                $newOrd->ord_nr = $orderService->createOrdNr();
                $newOrd->save();
            }
            Storage::put(('public/pdf/invoices/' . $year . '/' . $month . '/' . $invNr . '.pdf'), $contents);
        }
    }
}
