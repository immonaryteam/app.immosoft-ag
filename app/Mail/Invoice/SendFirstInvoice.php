<?php

namespace App\Mail\Invoice;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendFirstInvoice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $invNr;
    public $pathToFile;
    public $sal;

    public function __construct($invNr, $pathToFile, $sal)
    {
        $this->invNr = $invNr;
        $this->pathToFile = $pathToFile;
        $this->sal = $sal;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.invoice.first')
            ->attach($this->pathToFile)
            ->subject('Ihre Rechnung '.$this->invNr);
    }
}
