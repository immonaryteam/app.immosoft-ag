<?php

namespace App\Models\Order;

use App\Models\Product\Prod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    public function prod(){
        return $this->belongsTo(Prod::class);
    }
    public function ord(){
        return $this->belongsTo(Order::class);
    }
}
