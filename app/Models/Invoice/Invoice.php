<?php

namespace App\Models\Invoice;

use App\Models\Order\Order;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    public function ord(){
        return $this->belongsTo(Order::class);
    }
    public function statcode(){
        return $this->belongsTo(InvoiceStatusCode::class, 'inv_status_code_id');
    }

}
