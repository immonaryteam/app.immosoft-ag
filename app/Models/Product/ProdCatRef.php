<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdCatRef extends Model
{
    use HasFactory;

    public function prod()
    {
        return $this->belongsTo(Prod::class);
    }

    public function prodCat()
    {
        return $this->belongsTo(ProdCat::class);
    }
}
