<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prod extends Model
{
    use HasFactory;
    
    public function prodCat(){
        return $this->belongsTo(ProdCat::class);
    }

    public function prodDet(){
        return $this->hasOne(ProdDetail::class);
    }
}
