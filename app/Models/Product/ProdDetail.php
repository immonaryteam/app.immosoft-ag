<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdDetail extends Model
{
    use HasFactory;

    public function prod(){
        return $this->belongsTo(Prod::class);
    }
}
