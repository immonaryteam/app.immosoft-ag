<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdCatDetail extends Model
{
    use HasFactory;

    public function prodCat(){
        return $this->belongsTo(ProdCat::class);
    }
}
