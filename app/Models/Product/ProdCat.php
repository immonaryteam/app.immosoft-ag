<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdCat extends Model
{
    use HasFactory;

    public function prodCatDet(){
        return $this->hasOne(ProdCatDetail::class);
    }
}
