<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdSubRef extends Model
{
    use HasFactory;

    public function prod(){
        return $this->belongsTo(Prod::class);
    }

    public function prodSub(){
        return $this->belongsTo(Prod::class);
    }
}
