<?php

namespace App\Http\Controllers\Customer;


use Illuminate\Http\Request;
use App\Models\Customer\Customer;
use App\Models\Location\Location;
use App\Http\Controllers\Controller;
use App\Models\Customer\CustomerLocation;
use App\Models\Order\Order;
use App\Services\Custom\General\NotificationService;
use App\Services\Custom\Customer\CustomerCreateService;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::where('account_type', 'pri')->get();
        return view('parts.customers.index', compact('customers'));
    }

    public function create()
    {
        return view('parts.customers.create');
    }

    public function show($cusID)
    {
        $cus = Customer::find($cusID);


        $whereAndEntLoc = ['cus_id' => $cusID, 'is_billing' => 1];
        $cusLoc = CustomerLocation::where($whereAndEntLoc)->first();
        $locID = $cusLoc->loc_id;

        $loc = Location::find($locID);

        $orders = Order::where(['cus_id' => $cus->id, 'ord_status_code_id' => 1])->get();

        return view('parts.customers.show', compact('cus', 'loc', 'orders'));
    }

    public function store(Request $request, CustomerCreateService $customerCreateService, NotificationService $notificationService)
    {
        // check if its private or enterprise customer
        $selCusType = $request->selCusType;
        // create customer
        $customerData = $customerCreateService->getCustomerData($request, $selCusType);
        $customerID = $customerCreateService->createCustomer($customerData, $selCusType);
        $notification = $notificationService->setToastrNoti('Kunde erfolgreich erstellt', 'success');

        return redirect()->route('team.customer.billing-address.create', $customerID)->with($notification);
    }
}
