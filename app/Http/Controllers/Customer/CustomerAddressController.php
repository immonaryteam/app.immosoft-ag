<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer\CustomerLocation;
use App\Services\Custom\Location\LocationService;
use Illuminate\Http\Request;

class CustomerAddressController extends Controller
{
    public function create($cusID){
        return view('parts.customers.billing.create-billing-address', compact('cusID'));
    }

    public function store(Request $request, $cusID, LocationService $locService){
        //Check location if entry exists        
        $locID = $locService->locCheck($request);
        $locID = $locService->locAdd($request, $locID);

        $cusLoc = new CustomerLocation;
        $cusLoc->cus_id = $cusID;
        $cusLoc->loc_id = $locID;
        $cusLoc->is_billing = 1;
        $cusLoc->save();
        
        return redirect()->route('team.customer.show', $cusID);
    }
}
