<?php

namespace App\Http\Controllers\Enterprise;

use Illuminate\Http\Request;
use App\Models\Customer\Customer;
use App\Http\Controllers\Controller;
use App\Models\Enterprise\Enterprise;
use App\Models\Enterprise\EnterpriseCustomerRef;
use App\Services\Custom\Enterprise\EntCusRefService;
use App\Services\Custom\Customer\CustomerCreateService;

class EntContController extends Controller
{
    public function create(Request $request, $entID){
        $ent = Enterprise::find($entID);
        
        $inputTrue = $request->input('newOrd');
        
        if(empty($inputTrue)){
           $inputTrue = null; 
        }

        return view('parts.enterprises.contacts.create', compact('ent', 'inputTrue'));
    }

    public function store(Request $request, $entID, CustomerCreateService $customerCreateService, EntCusRefService $entCusRefService){
        // get the data needed for customer creation service
        $selCusType = $request->selCusType;
        
        // prepare the data
        $cusData = $customerCreateService->getCustomerData($request, $selCusType);
        // actual create the user
        $cusID = $customerCreateService->createCustomer($cusData, $selCusType);

        $entCusRefID = $entCusRefService->createEntCusRef($entID, $cusID);
    
        $checkNewOrder = $request->input('newOrd');
        if($checkNewOrder == 'true'){
            return redirect()->route('team.order.create');
        }else{
            return redirect()->route('team.enterprise.show', $entID);
        }
    }
}
