<?php

namespace App\Http\Controllers\Enterprise;

use Illuminate\Http\Request;
use App\Models\Location\Location;
use App\Http\Controllers\Controller;
use App\Models\Customer\Customer;
use App\Models\Enterprise\Enterprise;
use App\Models\Enterprise\EnterpriseCustomerRef;
use App\Models\Enterprise\EnterpriseLocation;
use App\Models\Enterprise\EnterpriseLocationReference;
use App\Models\Order\Order;
use App\Services\Custom\General\NotificationService;
use phpDocumentor\Reflection\Types\Null_;

class EnterpriseController extends Controller
{
    public function index()
    {
        $enterprises = Enterprise::all();

        return view('parts.enterprises.index', compact('enterprises'));
    }

    public function show($entID)
    {
        $enterprise = Enterprise::find($entID);

        $whereAndEntLoc = ['ent_id' => $entID, 'is_billing' => 1];
        $entLoc = EnterpriseLocation::where($whereAndEntLoc)->first();
        $locID = $entLoc->loc_id;

        $loc = Location::find($locID);

        $entCusRefCheck = EnterpriseCustomerRef::where('ent_id', $entID)->first();
        $entCusRef = EnterpriseCustomerRef::where('ent_id', $entID)->get();
        
        if ($entCusRefCheck == NULL) {
            $entCusRef = null;
            $entCus = null;
            $orders = array();
        } else {
            $entCus = array();
            // get all the customers related to the company (contacts)
            foreach ($entCusRef as $myRef) {
                $cusID = $myRef->cus_id;
                $customer = Customer::find($cusID);
                $entCus[] = $customer;
            }
            // get all open Orders
            foreach ($entCus as $cus) {
                $whereAnd = ['cus_id' => $cus->id, 'ord_status_code_id' => 1];
                $orders = Order::where($whereAnd)->get();
            }
        }
        return view('parts.enterprises.show', compact('enterprise', 'loc', 'entCus', 'orders'));
    }

    public function create()
    {
        return view('parts.enterprises.create');
    }

    public function store(Request $request, NotificationService $notificationService)
    {
        // check if enterprise exists
        $enterprise = Enterprise::where('ent_name', $request->entName)->first();

        // if not exist create
        if (empty($enterprise)) {
            $enterprise = new Enterprise;
            /* TODO:Add dynamic account id */
            $enterprise->ent_account_id = 1234;
            $enterprise->ent_name = $request->entName;
            $enterprise->ent_form = $request->entForm;
            $enterprise->is_active = 1;
            $enterprise->save();

            $enterpriseID = $enterprise->id;
        } else {
            $notification = $notificationService->setToastrNoti('Firma existiert bereits', 'alert');
            return redirect()->back()->with($notification);
        }
        // check for adress
        $whereAndAdress = ['zip' => $request->zip, 'place' => $request->place, 'street' => $request->street, 'street_nr' => $request->street_nr];
        /* TODO: REGEX Service for standardize  */
        $location = Location::where($whereAndAdress)->first();
        if (empty($location)) {
            $location = new Location;
            $location->zip = $request->zip;
            $location->place = $request->place;
            $location->street = $request->street;
            $location->street_nr = $request->streetNr;
            $location->save();

            $locationID = $location->id;
        } else {
            $locationID = $location->id;
        }
        $entLoc = new EnterpriseLocation;
        $entLoc->ent_id = $enterpriseID;
        $entLoc->loc_id = $locationID;
        if ($request->billingCheck == 1) {
            $entLoc->is_billing = 1;
        } else {
            $entLoc->is_billing = 0;
        }
        $entLoc->save();

        $notification = $notificationService->setToastrNoti('Firma erfolgreich erfasst', 'success');
        return redirect()->route('team.enterprise.show', $enterpriseID)->with($notification);
    }

    public function edit($enterpriseID)
    { }
}
