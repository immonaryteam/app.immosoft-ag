<?php

namespace App\Http\Controllers\Billing;

use App\Models\Order\Order;
use Illuminate\Http\Request;
use App\Models\Billing\Billing;
use App\Models\Invoice\Invoice;
use App\Http\Controllers\Controller;

use RealRashid\SweetAlert\Facades\Alert;
use App\Services\Custom\Invoice\InvNrService;

class BillingController extends Controller
{
    public function manualCreate($ordID, InvNrService $invNrSrvc){
        //TODO: Needed status of the order also
        $ord = Order::find($ordID);
    
        // invoice nr service
        $invNr = $invNrSrvc->createInvNr();
        //TODO: check if invoice exists
        $inv = new Invoice;
        $inv->inv_nr = $invNr;
        $inv->ord_id = $ordID;
        $inv->inv_status_code_id = 1;
        $inv->save();

        $ord = Order::find($ordID);
        $ord->ord_status_code_id = 4;
        $ord->save();

        toast('Rechnung wird erstellt, bitte warten...','success')->timerProgressBar();
        return redirect()->back();
    }
}
