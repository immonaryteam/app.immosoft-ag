<?php

namespace App\Http\Controllers\Invoice;

use PDF;
use App\Models\Order\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Invoice\Invoice;
use App\Models\Order\OrderItem;
use App\Models\Customer\Customer;
use App\Models\Location\Location;
use App\Http\Controllers\Controller;
use App\Mail\Invoice\SendFirstInvoice;
use App\Models\Enterprise\Enterprise;
use App\Models\Enterprise\EnterpriseLocation;
use App\Models\Enterprise\EnterpriseCustomerRef;
use App\Services\Custom\General\MonthService;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller
{
    public function index()
    {
        $invoices = Invoice::all();

        return view('parts.invoices.index', compact('invoices'));
    }

    public function show($invID)
    {
        $inv = Invoice::find($invID);
        // get the order
        $ordID = $inv->ord_id;
        $ord = Order::find($ordID);
        // get the commending customer ID
        $cusID = $ord->cus_id;
        $cus = Customer::find($cusID);
        // check if customer is business
        $entRefCheck = EnterpriseCustomerRef::where('cus_id', $cusID)->first();

        if (empty($entRefCheck)) {
            $check = 0;
            $cus = $cus;
            $ent = NULL;
            /* TODO: Invoice adress private */
            $loc = NULL;
        } else {
            $check = 1;
            $cus = NULL;
            $entID = $entRefCheck->ent_id;
            $ent = Enterprise::find($entID);
            $entLoc = EnterpriseLocation::where(['ent_id' =>  $entID, 'is_billing' => 1])->first();
            $entLocID = $entLoc->loc_id;
            $loc = Location::find($entLocID);
        }

        $items = OrderItem::where('ord_id', $ordID)->get();

        return view('parts.invoices.show', compact('inv', 'cus', 'ent', 'check', 'loc', 'items', 'ord'));
    }

    public function sendInitialInvoice($invID, MonthService $monthService)
    {
        $inv = Invoice::find($invID);
        // get the order
        $ordID = $inv->ord_id;
        $ord = Order::find($ordID);
        // get the commending customer ID
        $cusID = $ord->cus_id;
        $cus = Customer::find($cusID);
        // check if customer is business
        $entRefCheck = EnterpriseCustomerRef::where('cus_id', $cusID)->first();

        if (empty($entRefCheck)) {
            $check = 0;
            $cus = $cus;
            $ent = NULL;
            /* TODO: Invoice adress private */
            $loc = NULL;
        } else {
            $check = 1;
            $cus = NULL;
            $entID = $entRefCheck->ent_id;
            $ent = Enterprise::find($entID);
            $entLoc = EnterpriseLocation::where(['ent_id' =>  $entID, 'is_billing' => 1])->first();
            $entLocID = $entLoc->loc_id;
            $loc = Location::find($entLocID);
        }
        $specItemCheck = OrderItem::where(['ord_id' => $ordID, 'prod_id' => 57])->first();
        $otherItemCheck = OrderItem::where('ord_id', $ordID)->where('prod_id', '!=', 57)->first();

        $specPriceTot = 0;

        $specItems = OrderItem::where(['ord_id' => $ordID, 'prod_id' => 57])->get();
        $otherItems = OrderItem::where('ord_id', $ordID)->where('prod_id', '!=', 57)->get();

        $totalItems = OrderItem::where('ord_id', $ordID)->get();


        $priceTot = 0;
        foreach ($totalItems as $totalItem) {
            $singlePrice = $totalItem->ord_item_price;
            if ($totalItem->ord_item_quantity_type == "min") {
                $itemPrice = ($singlePrice / 60) * $totalItem->ord_item_quantity;;
            } elseif ($totalItem->ord_item_quantity_type = "pce") {
                $itemPrice = $singlePrice * $totalItem->ord_item_quantity;
            }
            $priceTot += $itemPrice;
        }
        $mwst = ($priceTot / 100) * 7.7;
        $mwst = round($mwst / .05, 0) * .05;
        /* dd($mwst); */

        $priceTot = $priceTot + $mwst;

        if (!empty($specItemCheck)) {
            foreach ($specItems as $specItem) {
                $price = $specItem->ord_item_price;
                $quantity = $specItem->ord_item_quantity;
                $type = $specItem->ord_item_quantity_type;

                if ($type == "min") {
                    $tempPrice = ($price / 60) * $quantity;
                } elseif ($type == "pce") {
                    $tempPrice = $price * $quantity;
                }

                $specPriceTot += $tempPrice;
            }
        }
        $specItemArtNr = 101;

        $now = Carbon::now()->toDateTimeString();
        $nowMt = Date('m', strtotime($now));
        $month = $monthService->getGermanMonth($nowMt);
        $day = Date('d', strtotime($now));

        ini_set('memory_limit', '2048M');
        $pdf = PDF::loadView('pdf.invoice', compact(
            'inv',
            'cus',
            'ent',
            'check',
            'loc',
            'specItemCheck',
            'specItemArtNr',
            'specPriceTot',
            'otherItems',
            'otherItemCheck',
            'ord',
            'month',
            'day',
            'mwst',
            'priceTot',
            'specItems'
        ));
        return $pdf->stream();
    }

    public function approveInvoice($invID, MonthService $monthService)
    {
        $now = Carbon::now()->toDateTimeString();
        $nowMt = Date('m', strtotime($now));
        $month = $monthService->getGermanMonth($nowMt);
        $day = Date('d', strtotime($now));
        $year = Date('Y', strtotime($now));

        $inv = Invoice::find($invID);

        $ord = Order::find($inv->ord_id);

        $cus = Customer::find($ord->cus_id);
        $cusMail = $cus->email;

        if ($cus->salutation == "male") {
            $sal = "Sehr geehrter Herr";
        } elseif ($cus->salutation == "female") {
            $sal = "Sehr geehrte Frau";
        } else {
            $sal = "Sehr geehrte, sehr geehrter Frau / Herr";
        }

        $cusName = $cus->lastname;

        $pathToFile = public_path('storage/pdf/invoices/'.$year.'/'.$month.'/'.$inv->inv_nr.'.pdf');
        
        Mail::to('projektmanagement@immosoft-ag.ch')->send(new SendFirstInvoice($inv->inv_nr, $pathToFile, $sal, $cusName));

        $inv->inv_status_code_id = 8;
        $inv->save();

    }
}
