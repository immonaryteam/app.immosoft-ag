<?php

namespace App\Http\Controllers\Order;

use App\Models\Order\Order;
use App\Models\Product\Prod;
use Illuminate\Http\Request;
use App\Models\Order\OrderItem;
use App\Models\Product\ProdCat;
use App\Models\Product\Product;
use App\Models\Customer\Customer;
use App\Models\Location\Location;
use App\Http\Controllers\Controller;
use App\Models\Enterprise\Enterprise;
use App\Models\Product\ProductCategory;
use App\Services\Custom\Order\OrderService;
use App\Models\Enterprise\EnterpriseLocation;
use App\Models\Enterprise\EnterpriseCustomerRef;
use App\Models\Product\ProdCatRef;
use App\Services\Custom\Location\LocationService;

class OrderController extends Controller
{
    //
    public function index()
    {
        return view('parts.order.index');
    }
    public function createType()
    {
        return view('parts.order.create.singleorreccuring');
    }
    public function storeType(Request $request)
    {
        $ordType = $request->singleOrReccurring;


        return redirect()->route('team.order.create', 'ordType=' . $ordType);
    }
    public function create()
    {
        $ordType = request()->ordType;
        $privateCustomers = Customer::where('account_type', 'pri')->get();
        $enterprises = Enterprise::all();
        return view('parts.order.create', compact('enterprises', 'privateCustomers', 'ordType'));
    }
    public function show($orderID)
    {
        $order = Order::find($orderID);
        $ordDetCheck = OrderItem::where('ord_id', $orderID)->first();
        //TODO:Service
        $categories = ProdCat::all();
        $prodData = array();
        $childs = array();
        foreach ($categories as $category) {
            $prodCatRef = ProdCatRef::where(['prod_cat_id' => $category->id])->get();
            $prods = array();
            foreach ($prodCatRef as $ref) {
                $prods[] = Prod::where('id', $ref->prod_id)->first();
            }
            foreach ($prods as $prod) {
                $prodData[] = [
                    'prod_id' => $prod->id,
                    'prod_pos' => $prod->prod_art_nr,
                    'prod_name' => $prod->prodDet->prod_name,
                    'prod_price' => $prod->prodDet->prod_price
                ];
            }
        }
        $ordDet = OrderItem::where('ord_id', $orderID)->get();
        // check if user is enterprise customer
        $cusID = $order->cus_id;
        $entCusRefCheck = EnterpriseCustomerRef::where('cus_id', $cusID)->first();

        // if customer exists in reference
        if (!empty($entCusRefCheck)) {
            $enterprise = Enterprise::where('id', $entCusRefCheck->ent_id)->first();
            // check for billing adress
            $whereAnd = ['ent_id' => $enterprise->id, 'is_billing' => 1];
            $entLoc = EnterpriseLocation::where($whereAnd)->first();
            $loc = Location::find($order->billing_loc_id);
        } else {
            //TODO: Private customer
            $locID = $order->billing_loc_id;
            $loc = Location::find($order->billing_loc_id);
        }
        return view('parts.order.show', compact('order', 'ordDetCheck', 'prodData', 'ordDet', 'loc'));
    }

    public function init(Request $request, OrderService $orderService)
    {
        $type = $request->account;
        $res = substr($type, 0, 4);
        if ($res == "ent.") {
            $entID = substr($type, strpos($type, ".") + 1);
            // check if any enterprise contact exists
            $entCusRefCheck = EnterpriseCustomerRef::where('ent_id', $entID)->first();
            if (empty($entCusRefCheck)) {
                //TODO: redirect if no reference to create customer first
                return redirect()->to('/firmen/' . $entID . '/kontakt-hinzufuegen?newOrd=true');
            } else {
                $ordType = request()->ordType;
                if ($ordType == "single") {
                    $reccFreq = null;
                    $ordType = 1;
                } else {
                    $reccFreq = $request->reccFreq;
                    $ordType = 2;
                }
                /* Create the order */
                $customer = Customer::find($entCusRefCheck->cus_id);
                $cusID = $customer->id;

                $ordID = $orderService->createOrder($cusID, $request, $reccFreq, $ordType);

                return redirect()->route('team.order.show', $ordID);
            }
        } elseif ($res = "pri.") {
            $ordType = request()->ordType;
            if ($ordType == "single") {
                $reccFreq = null;
                $ordType = 1;
            } else {
                $reccFreq = $request->reccFreq;
                $ordType = 2;
            }
            $cusID = substr($type, strpos($type, ".") + 1);
            $ordID = $orderService->createOrder($cusID, $request, $reccFreq, $ordType);

            return redirect()->route('team.order.show', $ordID);
        } else {
            abort(403);
        }
    }

    public function update(Request $request, $ordID, $type, OrderService $orderService)
    {
        // check if its add or delete
        if ($type == "add") {
            $prodID = $request->product_id;
            $updateDetail = $orderService->addOrderDetail($request, $ordID, $prodID);
        } elseif ($type == "remove") {
            $prodID = $request->itemDeleted;
            $updateDetail = $orderService->removeOrderDetail($ordID, $prodID);
        }
        return redirect(route('team.order.show', $ordID));
    }
    public function billAdd(Request $request, $ordID, LocationService $locService)
    {
        // check if adress entry exists
        // first get id if id exists
        $locID = $locService->locCheck($request);
        // then bind the id again
        $locID = $locService->locAdd($request, $locID);
        // check if user is enterprise customer
        $order = Order::find($ordID);
        $cusID = $order->ord_cus_id;
        $entCusRefCheck = EnterpriseCustomerRef::where('cus_id', $cusID)->first();
        if (empty($entCusRefCheck)) {
            $isCompany = 0;
            $ID = $cusID;
        } else {
            $isCompany = 1;
            $ID = $entCusRefCheck->ent_id;
        }
        // set the reference of the adress
        $reference = $locService->locReference($locID, $isCompany, $ID, 1);

        return redirect()->back();
    }

    public function delete($ordID)
    {
        $ord = Order::find($ordID);
        $ord->ord_status_code_id = 6;
        $ord->ord_total = 0;
        $ord->ord_extern_date = NULL;
        $ord->ord_intern_date = NULL;
        $ord->update();

        return redirect()->back();
    }
}
