<?php

namespace App\Http\Middleware\User;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $userID = Auth::user()->id;
        if (User::find($userID)->isTeam()) {
          return $next($request);
        }
        return redirect(403);
    }
}
