<?php

namespace App\Services\Custom\General;

class MonthService{
  public function getGermanMonth($month){
    switch($month){
      case '01':
        $month = 'Januar';
        break;
      case '02':
        $month = 'Februar';
        break;
      case '03':
        $month = 'März';
        break;
      case '04':
        $month = 'April';
        break;
      case '05':
        $month = 'Mai';
        break;
      case '06':
        $month = 'Juni';
        break;
      case '07':
        $month = 'Juli';
        break;
      case '08':
        $month = 'August';
        break;
      case '09':
        $month = 'September';
        break;
      case '10':
        $month = 'Oktober';
        break;
      case '11':
        $month = 'November';
        break;
      case '12':
        $month = 'Dezember';
        break;
    }
    return $month;
  }
}