<?php

namespace App\Services\Custom\General;

class NotificationService
{
  public function setToastrNoti($message, $alertType)
  {
    $notification = [
      'message' => $message,
      'alert-type' => $alertType
    ];

    return $notification;
  }
}
