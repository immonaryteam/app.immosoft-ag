<?php

namespace App\Services\Custom\Location;

use App\Models\Customer\Customer;
use App\Models\Customer\CustomerLocation;
use App\Models\Enterprise\Enterprise;
use App\Models\Enterprise\EnterpriseLocation;
use App\Models\Location\Location;

class LocationService
{

  public function locCheck($request)
  {
    // check for adress
    $whereAndAdress = ['zip' => $request->zip, 'place' => $request->place, 'street' => $request->street, 'street_nr' => $request->streetNr];
    /* TODO: REGEX Service for standardize  */
    $location = Location::where($whereAndAdress)->first();

    if ($location == NULL) {
      return $locID = NULL;
    } else {
      $locID = $location->id;
    }

    return $locID;
  }


  public function locAdd($request, $locCheck)
  {
    if (empty($locCheck)) {
      $location = new Location;
      $location->zip = $request->zip;
      $location->place = $request->place;
      $location->street = $request->street;
      $location->street_nr = $request->streetNr;
      $location->save();
      $locID = $location->id;
    } else {
      $locID = $locCheck;
    }
    return $locID;
  }

  public function locReference($locID, $isCompany, $ID, $isBilling)
  {
    if ($isCompany == 1) {
      $entLoc = EnterpriseLocation::where(['ent_id' => $ID, 'loc_id' => $locID])->first();
      if ($isBilling == 1) {
        $entLocBillCheck = EnterpriseLocation::where(['ent_id' => $ID, 'is_billing' => 1])->get();
        foreach ($entLocBillCheck as $check) {
          $check->is_billing = 0;
          $check->save();
        }
      }
      if (empty($entLoc)) {
        $entLoc = new EnterpriseLocation;
        $entLoc->ent_id = $ID;
        $entLoc->loc_id = $locID;
      }
      $entLoc->is_billing = $isBilling;
      $entLoc->save();

      return $entLoc;
    } else {
      $cusLoc = CustomerLocation::where(['cus_id' => $ID, 'loc_id' => $locID])->first();
      if (empty($cusLoc)) {
        $cusLoc = new CustomerLocation;
        $cusLoc->ent_id = $ID;
        $cusLoc->loc_id = $locID;
        $cusLoc->is_billing = $isBilling;
        $cusLoc->save();
      }
      return $cusLoc;
    }
  }
}
