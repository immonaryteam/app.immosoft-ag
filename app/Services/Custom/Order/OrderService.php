<?php

namespace App\Services\Custom\Order;

use App\Models\Customer\CustomerLocation;
use App\Models\Order\Order;
use App\Models\Product\Prod;
use Illuminate\Support\Carbon;
use App\Models\Invoice\Invoice;
use App\Models\Order\OrderItem;
use App\Models\Product\Product;
use App\Models\Location\Location;
use App\Models\Enterprise\Enterprise;
use App\Models\Enterprise\EnterpriseLocation;
use App\Models\Enterprise\EnterpriseCustomerRef;

class OrderService
{
  public function createOrdNr()
  {
    $invoiceCount = Order::whereDate('created_at', Carbon::today())->count();

    $day = Carbon::now();


    $date = $day->toDateTimeString();

    $todDate = date('d', strtotime($date));
    $todMonth = date('m', strtotime($date));
    $todYear = date('Y', strtotime($date));
    $todYear = substr($todYear, -2);

    $invCountPlus = $invoiceCount + 1;

    $invNr = 'AT'.$todDate . $todMonth . $todYear . $invCountPlus;

    return $invNr;
  }
  public function createOrder($cusID, $request, $reccFreq, $ordType)
  {

    // check if user is enterprise customer
    $entCusRefCheck = EnterpriseCustomerRef::where('cus_id', $cusID)->first();

    // if customer exists in reference
    if (!empty($entCusRefCheck)) {
      $enterprise = Enterprise::where('id', $entCusRefCheck->ent_id)->first();
      // check for billing adress
      $whereAnd = ['ent_id' => $enterprise->id, 'is_billing' => 1];
      $entLoc = EnterpriseLocation::where($whereAnd)->first();
      $loc = Location::find($entLoc->id);
    } else {
      //TODO: Private customer
      $whereAnd = ['cus_id' => $cusID, 'is_billing' => 1];
      $entLoc = CustomerLocation::where($whereAnd)->first();
      $loc = Location::find($entLoc->id);
    }
    $ord = new Order;
    $ord->cus_id = $cusID;
    $ord->ord_nr = $this->createOrdNr();
    $ord->ord_type_id = $ordType;
    $ord->recc_freq = $reccFreq;
    $ord->billing_loc_id = $loc->id;
    $ord->ord_extern_date = $request['dateExtern'];
    $ord->ord_intern_date = $request['dateIntern'];
    $ord->recc_freq = $reccFreq;

    $ord->save();

    $ordID = $ord->id;

    return $ordID;
  }

  public function addOrderDetail($request, $ordID, $prodID)
  {
    $ordItem = OrderItem::where(['ord_id' => $ordID, 'prod_id' => $prodID])->first();
    $prod = Prod::find($prodID);

    // specific for "fachsupport"
    if ($request->product_id == 57 || $request->product_id == 58) {
      $ordItem = new OrderItem;
      $ordItem->ord_id = $ordID;
      $ordItem->prod_id = $prodID;
      $ordItem->ord_item_quantity = $request->amount;
      $ordItem->ord_item_quantity_type = $request->amount_type;
      $ordItem->ord_item_price = ($prod->prodDet->prod_price == NULL) ? 0 : $prod->prodDet->prod_price;
      $ordItem->ord_item_desc = $request->itemCustomDesc;
      $ordItem->ord_item_price = $request->itemCustomPrice;
      $ordItem->save();
    }
    // check if orderdetail allready exists in this order table
    elseif (empty($ordItem)) {
      $ordItem = new OrderItem;
      $ordItem->ord_id = $ordID;
      $ordItem->prod_id = $prodID;
      $ordItem->ord_item_quantity = $request->amount;
      $ordItem->ord_item_quantity_type = $request->amount_type;
      $ordItem->ord_item_price = ($prod->prodDet->prod_price == NULL) ? 0 : $prod->prodDet->prod_price;
      /* $ordItem->ord_det_prod_unit = $request->amount_type; */
      $ordItem->save();
    } else {
      $ordItem->ord_item_price = ($prod->prodDet->prod_price == NULL) ? 0 : $prod->prodDet->prod_price;
      $ordItem->ord_item_quantity = $request->amount;
      $ordItem->ord_item_quantity_type = $request->amount_type;
      $ordItem->save();
    }
    $price = $ordItem->ord_item_price;
    $quantity = $ordItem->ord_item_quantity;
    // update the corresponding order
    $addOrRem = "add";
    $ordUpd = $this->updateOrder($ordID, $price, $addOrRem, $quantity);
  }
  public function removeOrderDetail($ordID, $prodID)
  {
    $prod = Prod::find($prodID);
    $price = $prod->prod_price;
    if ($price == null) {
      $price = 0;
    }
    $addOrRem = "rem";
    //TODO:Quantity
    $quantity = 0;
    $this->updateOrder($ordID, $price, $addOrRem, $quantity);
    //TODO:Check price
    $ordDet = OrderItem::where(['ord_id' => $ordID, 'prod_id' => $prodID])->first();
    $ordDet->delete();
    return redirect()->back();
  }
  public function updateOrder($ordID, $price, $addOrRem, $quantity)
  {
    $ord = Order::find($ordID);
    $items = OrderItem::where('ord_id', $ordID)->get();
    $price = 0;
    foreach ($items as $item) {
      if ($item->ord_item_quantity_type == "pce" || $item->ord_item_quantity_type == "day") {
        $itemPrice = $item->ord_item_price * $item->ord_item_quantity;
      } elseif ($item->ord_item_quantity_type == "min") {
        $itemPrice = ($item->ord_item_price / 60) * $item->ord_item_quantity;
      }
      $price += $itemPrice;
    }
    if ($addOrRem == "add") {
      $newPrice = $price;
    }
    if ($addOrRem == "rem") {
      $newPrice = $price;
    }
    $ord->ord_total = $newPrice;
    $ord->save();

    return $ord->id;
  }
}
