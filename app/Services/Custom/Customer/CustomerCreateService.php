<?php

namespace App\Services\Custom\Customer;

use App\Models\Customer\Customer;
use App\Models\Customer\CustomerSetting;

class CustomerCreateService
{
  public function getCustomerData($request, $selCusType)
  {

    $customerData = [];
    $customerData['salutation'] = $request->salutation;
    $customerData['firstname'] = $request->firstname;
    $customerData['lastname'] = $request->lastname;
    $customerData['email'] = $request->email;
    $customerData['mobile'] = $request->mobile;
    $customerData['landline'] = $request->landline;


    return $customerData;
  }

  public function createCustomer($customerData, $selCusType)
  {
    $customer = new Customer;
    //TODO: account_id dynamic
    if ($selCusType == "smeScreate") {
      $selCusType = "sme";
    }
    if ($selCusType == "priCreate") {
      $selCusType = "pri";
    }
    $customer->cus_account_id = 1234;
    $customer->account_type = $selCusType;
    $customer->salutation = $customerData['salutation'];
    $customer->firstname = $customerData['firstname'];
    $customer->lastname = $customerData['lastname'];
    $customer->email = $customerData['email'];
    $customer->mobile = $customerData['mobile'];
    $customer->landline = $customerData['landline'];
    $customer->save();

    $customerID = $customer->id;

    return $customerID;
  }
}
