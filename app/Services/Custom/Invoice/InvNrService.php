<?php

namespace App\Services\Custom\Invoice;

use Illuminate\Support\Carbon;
use App\Models\Invoice\Invoice;

class InvNrService{

  public function createInvNr(){
    $invoiceCount = Invoice::whereDate('created_at', Carbon::today())->count();

    $day = Carbon::now();


    $date = $day->toDateTimeString();

    $todDate = date('d', strtotime($date));
    $todMonth = date('m', strtotime($date));
    $todYear = date('Y', strtotime($date));
    $todYear = substr($todYear, -2);
    
    $invCountPlus = $invoiceCount + 1;

    $invNr = 'RG'.$todDate.$todMonth.$todYear.$invCountPlus;

    return $invNr;

  }

}
