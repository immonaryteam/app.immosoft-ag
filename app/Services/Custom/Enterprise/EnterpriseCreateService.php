<?php

namespace App\Services\Custom\Enterprise;
use App\Models\Enterprise\Enterprise;
use App\Models\Enterprise\EnterpriseSetting;
use App\Services\Custom\General\NotificationService;
use App\Models\Enterprise\EnterpriseCustomerReference;

class EnterpriseCreateService
{
  public function createEnterprise($entName, $entType, $notificationService)
  {
    $enterpriseExist = Enterprise::where('ent_name', $entName)->first();

    if (empty($enterpriseExist)) {
      $enterprise = new Enterprise;
      //TODO:Dynamic Account ID
      $enterprise->ent_account_id = "ABCDEFG";
      $enterprise->ent_name = $entName;
      $enterprise->ent_form = $entType;
      $enterprise->save();

      $enterpriseID = $enterprise->id;

      return $enterpriseID;
    } else {
      return $enterpriseID = NULL;
    }
  }
  public function createEnterpriseSetting($enterpriseID)
  {

    $entSetting = new EnterpriseSetting;
    $entSetting->ent_id = $enterpriseID;
    $entSetting->is_active = 1;
    $entSetting->save();

    $entSettingID = $entSetting->id;

    return $entSettingID;
  }

  public function createEntCusRef($customerID, $enterpriseID)
  {
    $entCusRef = new EnterpriseCustomerReference;
    $entCusRef->ent_customer_id = $customerID;
    $entCusRef->ent_id = $enterpriseID;
    $entCusRef->save();

    $entCusRefID = $entCusRef->id;

    return $entCusRefID;
  }
}
