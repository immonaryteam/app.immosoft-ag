<?php

namespace App\Services\Custom\Enterprise;

use App\Models\Enterprise\EnterpriseCustomerRef;

class EntCusRefService
{
  public function createEntCusRef($entID, $cusID)
  {
    // create the reference
    $entCusRef = new EnterpriseCustomerRef;
    $entCusRef->ent_id = $entID;
    $entCusRef->cus_id = $cusID;
    $entCusRef->save();

    $entCusRefID = $entCusRef->id;

    return $entCusRefID;
  }
}
