@component('mail::message')
# {{$sal}}

Anbei finden Sie die Rechnung für Ihre bezogenen Leistungen.

Wir bitten Sie, die Rechnung innert 30 Tagen zu begleichen.

Freundliche Grüsse<br/>
Immosoft AG
@endcomponent
