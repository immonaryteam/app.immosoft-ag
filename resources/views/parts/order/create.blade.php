@extends('layouts.app')

@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="card col-md-11 col-sm-12 mx-auto">
          <div class="card-body">
            <h2>Neuen Auftrag initialisieren</h2>
            <hr />
            <form action="{{route('team.order.init','ordType='.$ordType)}}" method="POST">
              @csrf
              <div class="row">
                @if($ordType == "single")
                <div class="form-group col-sm-12 col-md-6">
                  <label for="dateExtern">Auslieferdatum (Extern)</label>
                  <input type="datetime-local" class="form-control" name="dateExtern" id="dateExtern">
                </div>
                <div class="form-group col-sm-12 col-md-6">
                  <label for="dateIntern">Auslieferdatum (Intern)</label>
                  <input type="datetime-local" class="form-control" name="dateIntern" id="dateIntern">
                </div>
                @else
                <div class="form-group col-sm-12 col-md-6">
                  <label for="reccFreq">Frequenz für wiederkehrenden Auftrag eingeben</label>
                  <select class="form-control" name="reccFreq" id="reccFreq">
                    <option selected value="0">-- Bitte wählen --</option>
                    <option value="weekly">Wöchentlich</option>
                    <option value="monthly">Monatlich</option>
                    <option value="yearly">Jährlich</option>
                  </select>
                </div>
                @endif
                <div class="form-group col-sm-12 col-md-6">
                  <label for="account">Kunde wählen</label>
                  <select class="form-control" name="account" id="account">
                    <option selected value="0">-- Bitte wählen --</option>
                    @foreach ($privateCustomers as $customer)
                    <optgroup label="Privatkunden">
                      <option value="pri.{{$customer->id}}"> {{$customer->firstname}}</option>
                    </optgroup>
                    @endforeach
                    @foreach ($enterprises as $enterprise)
                    <optgroup label="Firmenkunden">
                      <option value="ent.{{$enterprise->id}}"> {{$enterprise->ent_name}}</option>
                    </optgroup>
                    @endforeach
                  </select>
                </div>
              </div>
              <button class="btn btn-secondary col-sm-12 col-md-6 col-lg-4"
                type="submit">{{__('Auftrag initialisieren')}}</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection