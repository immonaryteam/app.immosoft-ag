@extends('layouts.app')

@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="card col-md-11 col-sm-12 mx-auto">
          <div class="card-body">
            <h2>Art des Auftrags wählen</h2>
            <hr />
            <form action="{{route('team.order-type.store')}}" method="POST">
              @csrf
              <div class="row">
                <div class="form-group col-sm-12 col-md-5 mx-auto">
                  <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="single" name="singleOrReccurring" value="single">
                    <label class="custom-control-label" for="single">Einzelauftrag</label>
                  </div>
                </div>
                <div class="form-group col-sm-12 col-md-5 mx-auto">
                  <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="reccurring" name="singleOrReccurring" value="reccurring">
                    <label class="custom-control-label" for="reccurring">Wiederkehrender Auftrag</label>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-secondary">Los</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection