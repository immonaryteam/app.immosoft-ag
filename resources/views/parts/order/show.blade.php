@extends('layouts.app')

@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="card col-sm-12 col-md-6 mx-auto">
          <div class="card-body">
            <h2>Aufrag {{$order->ord_nr}}</h2>
            @if($order->ord_status_code_id == 4)
            <h5 style="color:green">In Rechnung gestellt</h5>
            @endif
            <hr />
            <h5>Rechnungsadresse</h5>
            <p>{{$loc->street}} {{$loc->street_nr}}<br />{{$loc->zip}} {{$loc->place}}</p>
            <hr />
            <h5>Bestellte Produkte</h5>
            <hr />
            @if(!empty($ordDetCheck))
            <div class="table-responsive">
              <table class="table table-borderless">
                <thead>
                  <tr>
                    <th>Art.-Nr.</th>
                    <th>Produktname</th>
                    <th>Menge</th>
                    <th>Einheit</th>
                    <th>Gesamtpreis</th>
                    <th>Bearbeiten</th>
                    <th>Löschen</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($ordDet as $det)
                  <tr>
                    <td>{{$det->prod->prod_art_nr}}</td>
                    @if($det->prod_id == 57)
                    <td>{{$det->ord_item_desc}}</td>
                    @else
                    <td>{{$det->prod->prodDet->prod_name}}</td>
                    @endif
                    <td align="right">{{$det->ord_item_quantity}}</td>
                    <td>@if($det->ord_item_quantity_type == "pce") Stück @elseif($det->ord_item_quantity_type == "min")
                      Minuten @elseif($det->ord_item_quantity_type == "day") Tage @endif
                    </td>
                    @if($det->prod_id == 57 || $det->prod_id == 58)
                    <td>
                      @if($det->ord_item_quantity_type == "min")
                      CHF {{number_format((($det->ord_item_price / 60) * $det->ord_item_quantity), 2, ',', "'")}}
                      @else
                      CHF {{number_format(($det->ord_item_price * $det->ord_item_quantity), 2, ',', "'")}}
                      @endif
                    </td>
                    @else
                    <td>{{number_format(($det->prod->prodDet->prod_price * $det->ord_item_quantity), 2, ',', "'")}}
                    </td>
                    @endif
                    <td>
                      <a href=""><i class="fas fa-edit fa-lg" style="color:black;"></i></a>
                    </td>
                    <td class="text-center">
                      <form action="{{route('team.order.update', [$order->id, 'remove'])}}" method="POST">
                        @csrf
                        <input type="hidden" name="itemDeleted" value="{{$det->prod_id}}">
                        <button type="submit" style="border: none;background-color:none;">
                          <i class="fas fa-trash-alt fa-lg" style="color: red;"></i>
                        </button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr style="border-top: 1px double black;">
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th>CHF {{number_format($order->ord_total), 2, ',', "'"}}</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            @else
            <p>Keine Auftragsdetails hinterlegt <a href="#addOrderDetailModal">jetzt loslegen</a></p>
            @endif
          </div>
        </div>
        <div class="card col-sm-12 col-md-5 mx-auto">
          <div class="card-body">
            <h2>Funktionen</h2>
            <hr />
            <p>
              <a class="btn btn-secondary btn-block" href="" data-toggle="modal"
                data-target="#addOrderDetailModal">Auftragsdetail hinzufügen</a>
            </p>
            <p>
              <a class="btn btn-secondary btn-block" href="" data-toggle="modal"
                data-target="#addBillingModal">Rechnungsadresse ändern/hinzufügen</a>
            </p>
            <p>
              <a class="btn btn-secondary btn-block" href="" data-toggle="modal" data-target="">Kontaktperson
                ändern/hinzufügen</a>
            </p>
            @if($order->ord_status_code_id != 4)
            <p>
            <form action="{{route('team.billing.create.manual', $order->id)}}" method="POST">
              @csrf
              <a class="btn btn-secondary btn-block" id="billSubmit">In Rechnung stellen</a>
            </form>

            </p>
            @endif
          </div>
          <div class="card-body">
            <form action="{{route('team.order.delete', $order->id)}}" method="POST">
              @csrf
              <a class="btn btn-danger btn-block" id="orderDel">Auftrag stornieren</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('parts.order.modal')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.4/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('form #orderDel').click(function(event) {
      let $form = $(this).closest('form');
      const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success col-sm-10 col-md-5',
                cancelButton: 'btn btn-danger col-sm-10 col-md-5'
            },
            buttonsStyling: true,
      });
      swalWithBootstrapButtons.fire({
            title: 'Auftrag stornieren?',
            text: "Soll der Auftrag wirklich storniert werden?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Stornieren',
            cancelButtonText: 'Abbrechen',
            reverseButtons: true
        }).then((result) => {
          if (result.isConfirmed) {    
            $form.submit();
          } else if (result.isDenied) {    
            Swal.fire('Rechnung wurde nicht erstellt', '', 'warning')  
          }
        });
        }
  
  )});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('form #orderDel').click(function(event) {
      let $form = $(this).closest('form');
      const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success col-sm-10 col-md-5',
                cancelButton: 'btn btn-danger col-sm-10 col-md-5'
            },
            buttonsStyling: true,
      });
      swalWithBootstrapButtons.fire({
            title: 'Auftrag stornieren?',
            text: "Soll der Auftrag wirklich storniert werden?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Stornieren',
            cancelButtonText: 'Abbrechen',
            reverseButtons: true
        }).then((result) => {
          if (result.isConfirmed) {    
            $form.submit();
          } else if (result.isDenied) {    
            Swal.fire('Rechnung wurde nicht erstellt', '', 'warning')  
          }
        });
        }
  
  )});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('form #billSubmit').click(function(event) {
      let $form = $(this).closest('form');
      const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success col-sm-10 col-md-5',
                cancelButton: 'btn btn-danger col-sm-10 col-md-5'
            },
            buttonsStyling: true,
      });
      swalWithBootstrapButtons.fire({
            title: 'Are you  sure?',
            text: "In Rechnung stellen?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
          if (result.isConfirmed) {    
            $form.submit();
          } else if (result.isDenied) {    
            Swal.fire('Rechnung wurde nicht erstellt', '', 'warning')  
          }
        });
        }
  
  )});


</script>
<script>
  $('#product_id').on('change', function(){
    if($(this).val() === "57" || $(this).val() === "58"){
      $('#fachsuppDiv').show();

    }else{
      $('#fachsuppDiv').hide();

    }
  });
</script>
@endsection