{{-- Add billing details modal --}}
<div class="modal fade" id="addBillingModal" tabindex="-1" aria-labelledby="addBillingModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addBillingModalLabel">Rechnungsadresse wählen / ändern</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('team.order.bill.add', $order->id)}}" method="POST">
        @csrf
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-sm-12 col-md-8 mx-auto">
              <label for="street">Strasse</label>
              <input type="text" class="form-control" placeholder="Strasse" name="street" id="street"
                value="{{old('street', $loc->street)}}">
            </div>
            <div class="form-group col-sm-12 col-md-3 mx-auto">
              <label for="streetNr">Hausnummer</label>
              <input type="text" class="form-control" placeholder="Nr." name="streetNr" id="streetNr"
                value="{{old('streetNr', $loc->street_nr)}}">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-sm-12 col-md-3 mx-auto">
              <label for="zip">Postleitzahl</label>
              <input type="number" class="form-control" min="1000" max="9999" placeholder="PLZ" name="zip" id="zip"
                value="{{old('zip', $loc->zip)}}">
            </div>
            <div class="form-group col-sm-12 col-md-8 mx-auto">
              <label for="place">Ortschaft</label>
              <input type="text" class="form-control" min="1000" max="9999" placeholder="Ort" name="place" id="place"
                value="{{old('place', $loc->place)}}">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Schliessen</button>
          <button type="submit" class="btn btn-primary">Rechnungsadresse speichern</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- Add products to order modal --}}
<div class="modal fade" id="addOrderDetailModal" tabindex="-1" aria-labelledby="addOrderDetailModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addOrderDetailModalLabel">Auftragsdetail hinzufügen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('team.order.update', [$order->id, 'add'])}}" method="POST">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="">Produkt</label>
            <select name="product_id" id="product_id" class="form-control">
              <option selected value="0">-- Bitte wählen --</option>
              @foreach ($prodData as $prod)
              <option value="{{$prod['prod_id']}}">Art.Nr: {{$prod["prod_pos"]}} | {{$prod["prod_name"]}}</option>
              @endforeach
            </select>
          </div>
          <div class="row">
            <div class="form-group col-sm-8">
              <label>Stückzahl / Zeiteinheit</label>
              <input type="number" name="amount" placeholder="Stückzahl / Zeiteinheit" class="form-control">
            </div>
            <div class="form-group col-sm-4">
              <label for="">Mengeneinheit</label>
              <select name="amount_type" id="amount_type" class="form-control">
                <option value="0">-- Bitte wählen</option>
                <option value="pce">Stück</option>
                <option value="min">Minuten</option>
                <option value="day">Tage</option>
              </select>
            </div>
          </div>
          <div id="fachsuppDiv" style="display:none;">
            <div class="row">
              <div class="form-group col-sm-8">
                <label>Beschreibung</label>
                <input type="text" name="itemCustomDesc" id="itemCustomDesc"
                  placeholder="Name der Dienstleistung" class="form-control">
              </div>
              <div class="form-group col-sm-4">
                <label>Preis (Stunden- oder Stückpreis)</label>
                <input type="number" step=".01" name="itemCustomPrice" id="itemCustomPrice"
                  placeholder="Preis pro Stunde bzw. Stück" class="form-control">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

