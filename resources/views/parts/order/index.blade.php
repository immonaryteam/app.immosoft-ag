@extends('layouts.app')

@section('content')

<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="card col-md-7 col-sm-12 mx-auto">
          <div class="card-body">
            <h2>Offene Aufträge</h2>
            <hr />
            <div class="table-responsive">
              <table class="table table-borderless">
                <thead>
                  <tr>
                    <th>Kunde</th>
                    <th>Datum Auslieferung</th>
                    <th>Zuständiger Mitarbeiter</th>
                    <th>Letztes Update</th>
                    <th>Details</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><a href="">Max Müller</a></td>
                    <td>08.08.1988</td>
                    <td><a href="">Manuel Steiner (mst)</a></td>
                    <td>Vor 2 Stunden</td>
                    <td><a href="">Anzeigen</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="card col-md-4 col-sm-12 mx-auto">
          <div class="card-body">
            <h2>Aktionen</h2>
            <hr />
            <a href="{{route('team.order.create')}}" class="btn btn-secondary btn-block">Neuen Auftrag initialisieren</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection