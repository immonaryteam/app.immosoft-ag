@extends('layouts.app')
@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="card col-sm-12 col-md-7 mx-auto">
          <div class="card-body">
            <h2 class="">Alle Aufträge</h2>
            <table class="table table-borderless">
              <thead>
                <tr>
                  <th>Auftragsnummer</th>
                  <th>Gesamtbetrag</th>
                  <th>Rechnungsstatus</th>
                  <th>Details</th>
                  </th>
              </thead>
              <tbody>
                @foreach ($invoices as $invoice)
                <tr>
                  <td>
                    {{$invoice->inv_nr}}
                  </td>
                  <td>
                    CHF {{$invoice->ord->ord_total}}
                  </td>
                  <td>
                    {{$invoice->statcode->inv_status_desc}}
                  </td>
                  <td><a href="{{route('team.invoices.show', $invoice->id)}}">Anzeigen</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card col-sm-12 col-md-4 mx-auto">
          <div class="card-body">
            <h2 class="">Aktionen</h2>
            <hr/>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection