@extends('layouts.app')
@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="card col-sm-12 col-md-7 mx-auto">
          <div class="card-body">
            <h2 class="">Rechnung {{$inv->inv_nr}}</h2>
            <h6>Status: {{$inv->statcode->inv_status_desc}}</h6>
            <hr />
            <h5>Auftraggeber</h5>
            @if($check == 1)
            <p>{{$ent->ent_name}}</p>
            <hr />
            <p>
              {{$loc->street}} {{$loc->street_nr}}, {{$loc->zip}} {{$loc->place}}</p>
            {{-- TODO: add private customer --}}
            @endif
            <div class="table-responsive">
              <table class="table table-borderless">
                <thead>
                  <tr>
                    <th>Art.-Nr.</th>
                    <th>Produktname</th>
                    <th>Menge</th>
                    <th>Einheit</th>
                    <th>Gesamtpreis</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($items as $item)
                  <tr>
                    <td>{{$item->prod->prod_art_nr}}</td>
                    @if($item->prod_id == 57)
                    <td>{{$item->ord_item_desc}}</td>
                    @else
                    <td>{{$item->prod->prodDet->prod_name}}</td>
                    @endif
                    <td align="right">{{$item->ord_item_quantity}}</td>
                    <td>@if($item->ord_item_quantity_type == "pce") Stück @else Minuten @endif</td>
                    @if($item->prod_id == 57)
                    <td>
                      @if($item->ord_item_quantity_type == "min")
                      CHF {{number_format((($item->ord_item_price / 60) * $item->ord_item_quantity), 2, ',', "'")}}
                      @else
                      CHF {{number_format(($item->ord_item_price * $item->ord_item_quantity), 2, ',', "'")}}
                      @endif
                    </td>
                    @else
                    <td>{{number_format(($item->prod->prodDet->prod_price * $item->ord_item_quantity), 2, ',', "'")}}
                    </td>
                    @endif
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr style="border-top: 1px double black;">
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th>CHF {{number_format($inv->ord->ord_total)}}</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        <div class="card col-sm-12 col-md-4 mx-auto">
          <div class="card-body">
            <h2 class="">Aktionen</h2>
            <hr />
            @if($inv->inv_status_code_id == 1)
              <a href="{{route('team.invoices.approve', $inv->id)}}" class="btn btn-secondary btn-block">Rechnung freigeben und versenden</a>
            @endif
            {{--             @if($inv->inv_status_code_id == 1)
            <a href="{{route('team.invoices.send.initial', $inv->id)}}" class="btn btn-secondary btn-block">Rechnung
            versenden</a>
            @endif --}}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection