@extends('layouts.app')
@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="card col-md-11 col-sm-12 mx-auto">
          <div class="card-body">
            <h2>Neuen Kontakt zu Firma {{$ent->ent_name}} erfassen</h2>
            <hr />
            <form action="{{route('team.enterprise.contact.store', $ent->id).'?newOrd='.$inputTrue }}" method="POST">
              @csrf
              <input type="hidden" name="selCusType" id="selCusType" value="smeCreate">
              <div class="row">
                <div class="form-group col-sm-12">
                  <label for="salutation">Anrede</label>
                  <select class="form-control" name="salutation" id="salutation" name="salutation">
                    <option selected value="0">-- Bitte wählen --</option>
                    <option value="male">Herr</option>
                    <option value="female">Frau</option>
                    <option value="other">Andere</option>
                  </select>
                </div>
                <div class="form-group col-sm-12 col-md-6">
                  <label for="firstname">Vorname</label>
                  <input type="text" class="form-control" name="firstname" id="firstname">
                </div>
                <div class="form-group col-sm-12 col-md-6">
                  <label for="lastname">Nachname</label>
                  <input type="text" class="form-control" name="lastname" id="lastname">
                </div>
                <div class="form-group col-sm-12 col-md-6">
                  <label for="email">E-Mail</label>
                  <input type="email" class="form-control" name="email" id="email">
                </div>
                <div class="form-group col-sm-12 col-md-6">
                  <label for="mobile">Mobil</label>
                  <input type="number" class="form-control" name="mobile" id="mobile">
                </div>
                <div class="form-group col-sm-12 col-md-6">
                  <label for="landline">Festnetz</label>
                  <input type="number" class="form-control" name="landline" id="landline">
                </div>

              </div>
              <button class="btn btn-secondary col-sm-12 col-md-6 col-lg-4"
                type="submit">{{__('Auftrag initialisieren')}}</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection