@extends('layouts.app')
@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <h2>Kunden erfassen</h2>
      <div class="card">
        <form action="{{route('team.enterprise.store')}}" method="POST">
          @csrf
          <div class="card-body" id="smeCreate">
            <h3>Firmenkunde hinzufügen</h3>
            <div class="row">
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="entName">Firmenname</label>
                  <input type="text" class="form-control" id="entName" name="entName">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="entForm">Unternehmensform</label>
                  <select class="form-control" name="entForm" id="entForm" name="entForm">
                    <option selected value="0">-- Bitte wählen --</option>
                    <option value="single">Einzelunternehmen</option>
                    <option value="ag">AG</option>
                    <option value="gmbh">GmbH</option>
                    <option value="commandit">Kommanditgesellschaft</option>
                    <option value="collective">Kollektivgesellschaft</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div class="row">
              <div class="col-sm-8 col-md-8">
                <div class="form-group">
                  <label for="street">Strasse</label>
                  <input type="text" class="form-control" name="street" id="street">
                </div>
              </div>
              <div class="col-sm-4 col-md-4">
                <div class="form-group">
                  <label for="streetNr">Hausnummer</label>
                  <input type="text" class="form-control" name="streetNr" id="streetNr">
                </div>
              </div>
              <div class="col-sm-4 col-md-4">
                <div class="form-group">
                  <label for="zip">Postleitzahl</label>
                  <input type="number" class="form-control" id="zip" name="zip">
                </div>
              </div>
              <div class="col-sm-8 col-md-8">
                <div class="form-group">
                  <label for="place">Ort</label>
                  <input type="text" class="form-control" id="place" name="place">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="billingCheck" name="billingCheck" value="1" checked>
                  <label for="billingCheck" class="form-check-label">Anschrift gleich wie Rechnungsadresse?</label>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-secondary col-sm-12 col-md-6 offset-md-3">Firma erstellen</button>
        </form>
      </div>
    </div>
  </div>
</div>
@push('footer-scripts')
<script>
  @if(Session::has('message'))
      var type = "{{ Session::get('alert-type', 'info') }}";
      switch(type){
          case 'info':
              toastr.info("{{ Session::get('message') }}");
              break;
  
          case 'warning':
              toastr.warning("{{ Session::get('message') }}");
              break;
  
          case 'success':
              toastr.success("{{ Session::get('message') }}");
              break;
  
          case 'error':
              toastr.error("{{ Session::get('message') }}");
              break;
      }
    @endif
</script>
@endpush
@endsection