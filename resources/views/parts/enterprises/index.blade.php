@extends('layouts.app')
@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="card col-sm-12 col-md-7 mx-auto">
          <div class="card-body">
            <h2 class="">Firmenliste</h2>
            <table class="table table-borderless">
              <thead>
                <tr>
                  <th>Name Firma</th>
                  <th>Rechtsform</th>
                  <th>Details</th>
                  </th>
              </thead>
              <tbody>
                @foreach ($enterprises as $enterprise)
                <tr>
                  <td>
                    {{$enterprise->ent_name}}
                  </td>
                  <td>
                    @if($enterprise->ent_form == "single")
                    Einzelunternehmen
                    @elseif($enterprise->ent_form == "ag")
                    AG
                    @elseif($enterprise->ent_form == "gmbh")
                    GmbH
                    @elseif($enterprise->ent_form == "commandit")
                    Kommanditgesellschaft
                    @elseif($enterprise->ent_form == "collective")
                    Kollektivgesellschaft
                    @else
                    <span style="color: red;font-weight:700;">Nicht gewählt</span>
                    @endif
                  </td>
                  <td><a href="{{route('team.enterprise.show', $enterprise->id)}}">Anzeigen</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card col-sm-12 col-md-4 mx-auto">
          <div class="card-body">
            <h2 class="">Aktionen</h2>
            <hr/>
            <a href="{{route('team.enterprise.create')}}" class="btn btn-secondary btn-block">Firma erfassen</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection