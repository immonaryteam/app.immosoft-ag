@extends('layouts.app')
@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <h2>Rechnungsadresse erfassen</h2>
      <div class="card">
        <form action="{{route('team.customer.billing-address.store', $cusID)}}" method="POST">
          @csrf
          <div class="card-body">
            <hr />
            <div class="row">
              <div class="col-sm-8 col-md-8">
                <div class="form-group">
                  <label for="street">Strasse</label>
                  <input type="text" class="form-control" name="street" id="street">
                </div>
              </div>
              <div class="col-sm-4 col-md-4">
                <div class="form-group">
                  <label for="streetNr">Hausnummer</label>
                  <input type="text" class="form-control" name="streetNr" id="streetNr">
                </div>
              </div>
              <div class="col-sm-4 col-md-4">
                <div class="form-group">
                  <label for="zip">Postleitzahl</label>
                  <input type="number" class="form-control" id="zip" name="zip">
                </div>
              </div>
              <div class="col-sm-8 col-md-8">
                <div class="form-group">
                  <label for="place">Ort</label>
                  <input type="text" class="form-control" id="place" name="place">
                </div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-secondary col-sm-12 col-md-6 offset-md-3">Rechnungsadresse erstellen</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection