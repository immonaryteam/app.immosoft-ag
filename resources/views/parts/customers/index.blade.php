@extends('layouts.app')

@section('content')

<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="card col-md-7 col-sm-12 mx-auto">
          <div class="card-body">
            <h2 class="">Kundenliste Privatkunden</h2>
            <table class="table table-borderless">
              <thead>
                <tr>
                  <th>Voller Name</th>
                  <th>Status</th>
                  <th>Accounttyp</th>
                  <th>Details</th>
              </thead>
              <tbody>
                @foreach ($customers as $customer)
                <tr>
                  <td>
                    @if($customer->salutation == "male")
                    <a href="">Herr {{$customer->firstname}} {{$customer->lastname}}</a>
                    @elseif($customer->salutation == "female")
                    <a href="">Frau {{$customer->firstname}} {{$customer->lastname}}</a>
                    @else
                    <a href="">Andere {{$customer->firstname}} {{$customer->lastname}}</a>
                    @endif
                  </td>
                  @if($customer->is_active == 1)
                  <td style="color:green;font-weight:700;">
                    Aktiv
                  </td>
                  @else
                  <td style="color: red;font-weight:700;">
                    Inaktiv
                  </td>
                  @endif
                  <td>
                    {{$customer->account_type}}
                  </td>
                  <td><a href="{{route('team.customer.show', $customer->id)}}">Anzeigen</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card col-md-4 col-sm-12 mx-auto">
          <div class="card-body">
            <h2>Aktionen</h2>
            <hr/>
            <a href="{{route('team.customer.create')}}" class="btn btn-secondary btn-block">Privatkunden erfassen</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection