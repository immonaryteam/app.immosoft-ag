@extends('layouts.app')

@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <div class="card">
        <div class="card-body">
          {{-- <h2>{{$enterprise->ent_name}}</h2> --}}
          <div class="row">
            <div class="card col-sm-12 col-md-7 mx-auto mb-5">
              <h3 class="mt-3">Details</h3>
              <hr />
              <div class="col-sm-12 col-md-8">
                <p>{{$cus->salutation}} {{$cus->firstname}} {{$cus->lastname}}</p>
                <h5>Rechnungsadresse</h5>
                <p>{{$loc->street}} {{$loc->street_nr}}, {{$loc->zip}} {{$loc->place}} </p>
                <h5>Pendente Aufträge</h5>
                <table class="table table-borderless">
                  <thead>
                    <tr>
                      <th>Auftragsnummer</th>
                      <th>Auftrag anzeigen</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($orders as $order)
                    <tr>
                      <td>{{$order->ord_nr}}</td>
                      @if($order->ord_type_id == 1)
                      <td>{{date('d.m.Y', strtotime($order->ord_extern_date))}} | {{date('H:i', strtotime($order->ord_extern_date))}} Uhr</td>
                      <td>{{date('d.m.Y', strtotime($order->ord_intern_date))}} | {{date('H:i', strtotime($order->ord_intern_date))}} Uhr</td>
                      @endif
                      <td><a href="{{route('team.order.show', $order->id)}}" class="btn btn-secondary btn-block">Los</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card col-sm-12 col-md-4 mx-auto">
              <div class="card-body">
                <h3>Aktionen</h3>
                <hr />
                <p>Bearbeiten</p>
                <hr />
                <a href="" class="btn btn-secondary btn-block">Firma bearbeiten</a>
                <a href="" class="btn btn-secondary btn-block">Rechnungsadresse ändern</a>
                <p class="mt-3">Erfassung</p>
                <hr />
                <a href="{{route('team.order-type.create')}}" class="btn btn-secondary btn-block">Auftrag erfassen</a>
                <a href="" class="btn btn-secondary btn-block">Kontaktperson erfassen</a>
                <a href="" class="btn btn-secondary btn-block">Notiz erfassen</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection