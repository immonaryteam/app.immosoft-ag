@extends('layouts.app')
<style>

</style>
@section('content')
<div class="container-fluid col-md-8">
  <div class="card">
    <div class="card-body">
      <h2>Kunden erfassen</h2>
      <div class="card">
        <form action="{{route('team.customer.store')}}" method="POST">
          @csrf
{{--           <div class="card-body">
            <div class="col-md-6 col-sm-12 mx-auto">
              <div class="selectionWrapper col-sm-12">
                <input type="radio" name="selCusType" id="priCus" value="priCreate" checked>
                <input type="radio" name="selCusType" id="smeCus" value="smeCreate">
                <label for="priCus" class="option priCus">
                  <div class="dot"></div>
                  <span>Privatkunde</span>
                </label>
                <label for="smeCus" class="option smeCus">
                  <div class="dot"></div>
                  <span>Geschäftskunde</span>
                </label>
              </div>
            </div>
          </div> --}}
          <div class="card-body" id="priCreate">
            <h3>Privatkunden hinzufügen</h3>
            <hr/>
            <input type="hidden" name="selCusType" id="selCusType" value="priCreate">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="salutation">Anrede</label>
                  <select class="form-control" id="salutation" name="salutation">
                    <option selected value="0">-- Bitte wählen --</option>
                    <option value="male">Herr</option>
                    <option value="female">Frau</option>
                    <option value="other">Andere</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="firstname">Vorname</label>
                  <input type="text" class="form-control" id="firstname" name="firstname">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="lastname">Nachname</label>
                  <input type="text" class="form-control" id="lastname" name="lastname">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="mobile">Mobil</label>
                  <input type="number" class="form-control" id="mobile" name="mobile">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="landline">Festnetz</label>
                  <input type="number" class="form-control" id="landline" name="landline">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="email">E-Mail</label>
                  <input type="email" class="form-control" id="email" name="email">
                </div>
              </div>
            </div>
          </div>
          {{-- <div class="card-body" id="smeCreate" style="display: none">
            <h3>Firmenkunde hinzufügen</h3>
            <div class="row">
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="entName">Firmenname</label>
                  <input type="text" class="form-control" id="entName" name="entName">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="entForm">Unternehmensform</label>
                  <select class="form-control" name="entForm" id="entForm" name="entForm">
                    <option selected value="0">-- Bitte wählen --</option>
                    <option value="single">Einzelunternehmen</option>
                    <option value="ag">AG</option>
                    <option value="gmbh">GmbH</option>
                    <option value="commandit">Kommanditgesellschaft</option>
                    <option value="collective">Kollektivgesellschaft</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="entSalutation">Anrede</label>
                  <select class="form-control" id="entSalutation" name="entSalutation">
                    <option selected value="0">-- Bitte wählen --</option>
                    <option value="male">Herr</option>
                    <option value="female">Frau</option>
                    <option value="other">Andere</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="entFirstname">Vorname</label>
                  <input type="text" class="form-control" id="entFirstname" name="entFirstname">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="entLastname">Nachname</label>
                  <input type="text" class="form-control" id="entLastname" name="entLastname">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="entMobile">Mobil</label>
                  <input type="number" class="form-control" id="entMobile" name="entMobile">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="entLandline">Festnetz</label>
                  <input type="number" class="form-control" id="entLandline" name="entLandline">
                </div>
              </div>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="entEmail">E-Mail</label>
                  <input type="email" class="form-control" id="entEmail" name="entEmail">
                </div>
              </div>
            </div>
          </div> --}}
          <button type="submit" class="btn btn-secondary col-sm-12 col-md-6 offset-md-3">Kunden erstellen, mit
            Rechnungsadresse fortfahren</button>
        </form>
      </div>
    </div>
  </div>
</div>
@push('footer-scripts')
<script>
  @if(Session::has('message'))
      var type = "{{ Session::get('alert-type', 'info') }}";
      switch(type){
          case 'info':
              toastr.info("{{ Session::get('message') }}");
              break;
  
          case 'warning':
              toastr.warning("{{ Session::get('message') }}");
              break;
  
          case 'success':
              toastr.success("{{ Session::get('message') }}");
              break;
  
          case 'error':
              toastr.error("{{ Session::get('message') }}");
              break;
      }
    @endif
</script>
@endpush
@endsection