<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Rechnung {{$inv->inv_nr}}</title>
  <style>
    /** Define the margins of your page **/
    @page {
      margin: 95px 95px;
    }

    html,
    body {
      font-family: DejaVu Sans;
      font-size: 13px;
    }

    header {
      position: fixed;
      top: -60px;
      left: 0px;
      right: 0px;
      height: 100px;

      /** Extra personal styles **/
      color: black;
      text-align: right;
    }

    footer {
      position: fixed;
      bottom: -60px;
      left: 0px;
      right: 0px;
      height: 75px;

      /** Extra personal styles **/
      color: black;
    }
  </style>
</head>

<body>
  <!-- Define header and footer blocks before your content -->
  <header>
    <img src="{{asset('img/logo/logo.jpg')}}" style="width: 150px;height:auto;">
  </header>

  <footer>
    <table style="width: 100%;">
      <tbody style="font-size: 11px;">
        <tr>
          <td align="left">Immosoft AG<br />Schlachthofstrasse 1<br />CH-8406 Winterthur</td>
          <td align="right">Tel: 043 433 03 59<br />Web: www.immosoft-ag.ch<br />&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </footer>

  <!-- Wrap the content of your PDF inside a main tag -->
  <main>
    <div style="page-break-after: always;margin-top:50px;">
      <p>
        <span style="text-decoration: underline;font-size:10px;letter-spacing:0.5px;">Immosoft AG, Schlachthofstrasse
          1, 8406 Winterthur</span><br />
        @if($check == 1)
        <span style="font-weight: 600;">{{$ent->ent_name}}</span><br />
        {{$loc->street}} {{$loc->street_nr}}<br />
        CH-{{$loc->zip}} {{$loc->place}}
        @else

        @endif
      </p>
      <p style="text-align: right">
        Winterthur, {{$day}}. {{$month}} 2021
      </p>
      <div style="margin-top: 50px;">
        <h3 style="text-align: left">Rechnung {{$inv->inv_nr}}</h3>
      </div>
      <p>
        Gemäss Ihrem Auftrag erlauben wir uns, Ihnen für unsere erbrachten Leistungen wie folgt Rechnung zu stellen:
      </p>
      <table style="width: 100%;">
        <tbody style="border-bottom: 1px solid black;">
          <tr>
            <td style="padding-bottom:10px;">
              Artikel-Nr.
            </td>
            <td style="padding-bottom:10px;">
              Bezeichnung
            </td>
            <td style="padding-bottom:10px;" align="right">
              CHF
            </td>
          </tr>
        </tbody>
        <tbody style="border-bottom: 1px solid black;">
          <tr>
            <td style="padding-top:10px;"></td>
            <td style="padding-top:10px;"></td>
            <td style="padding-top:10px;"></td>
          </tr>
          @if(!empty($specItemCheck))
          <tr>
            <td>
              {{number_format($specItemArtNr, 2, '.', "'")}}
            </td>
            <td>
              Fachsupport
            </td>
            <td align="right">
              {{number_format($specPriceTot, 2, '.', "'")}}
            </td>
          </tr>
          @endif
          @if(!empty($portalItemCheck))
          <tr>
            <td>
              {{number_format($portalItemArtNr, 2, '.', "'")}}
            </td>
            <td>
              Export auf Immobilienportale
            </td>
            <td align="right">
              {{number_format($portalPriceTot, 2, '.', "'")}}
            </td>
          </tr>
          @endif
          @if(!empty($otherItemCheck))
          @foreach ($otherItems as $item)
          @if($item->prod_id != 58)
          <tr>
            <td>{{$item->prod->prod_art_nr}}</td>
            <td>{{$item->prod->prodDet->prod_name}}</td>
            <td align="right">{{$item->prod->prodDet->prod_price}}</td>
          </tr>
          @endif
          @endforeach
          @endif
          <tr>
            <td style="padding-bottom:10px;"></td>
            <td style="padding-bottom:10px;"></td>
            <td style="padding-bottom:10px;"></td>
          </tr>
        </tbody>
        <tbody style="border-bottom: 3px solid black;">
          <tr>
            <td style="padding-top:10px;padding-bottom:10px;" align="left">7.7% MwSt.</td>
            <td></td>
            <td style="padding-top:10px;padding-bottom:10px;" align="right">{{number_format($mwst, 2, '.', "'")}}</td>
          </tr>
        </tbody>
        <tbody style="border-bottom: 3px solid black;">
          <tr>
            <td style="padding-top: 10px;padding-bottom:10px;" align="left"><strong>Total</strong></td>
            <td></td>
            <td style="padding-top: 10px;padding-bottom:10px;" align="right">{{number_format($priceTot, 2, '.', "'")}}
            </td>
          </tr>
        </tbody>
      </table>
      <p>
        Wir bedanken uns für den Auftrag und bitte Sie, den Nettobetrag innerhalb von 30 Tagen wie folgt zu
        überweisen:<br />
      </p>
      <p>
        Bank: Thurgauer Kantonalbank<br />
        IBAN: CH17 0078 4296 7693 2200 1<br />
        BIC: KBTGCH22<br />
        Kontoinhaber: Immosoft AG, Schlachthofstrasse 1, 8406 Winterthur
      </p>
      <p style="margin-top: 65px;">
        Freundliche Grüsse<br />
        Immosoft AG<br />
        (Anzeige ohne Unterschrift)
      </p>
    </div>
    @if(!empty($specItemCheck))
    <div style="page-break-after: never;margin-top:50px;">
      <table style="width: 100%">
        <tbody style="border-bottom: 1px solid black;">
          <tr>
            <td style="padding-bottom:10px;">
              Bezeichnung
            </td>
            <td style="padding-bottom:10px;" align="right">
              CHF
            </td>
          </tr>
        </tbody>
        @if(!empty($specItemCheck))
        <tbody style="border-bottom: 1px solid black;">
          <tr>
            <th align="left" style="padding-bottom: 10px;">Fachsupport</th>
          </tr>
          @foreach ($specItems as $item)
          <tr>
            <td align="left">{{$item->ord_item_desc}}</td>
            @if($item->ord_item_quantity_type == "pce")
            <?php
            $calcPrice = $item->ord_item_price * $item->ord_item_quantity;
            ?>
            <td align="right">{{number_format($calcPrice, 2, '.', "'")}}</td>
            @elseif($item->ord_item_quantity_type == "min")
            <?php 
            $calcPrice = $item->ord_item_price / 60 * $item->ord_item_quantity;
            ?>
            <td align="right">{{number_format($calcPrice, 2, '.', "'")}}</td>
            @endif
          </tr>
          @endforeach
        </tbody>
        <tbody>
          <tr>
            <td style="padding-bottom:10px;"></td>
            <td style="padding-bottom:10px;"></td>
          </tr>
        </tbody>
        <tbody>
          <tr>
            <td align="left">Subtotal</td>
            <td align="right">{{number_format($specPriceTot, 2, '.', "'")}}</td>
          </tr>
        </tbody>
        <tbody style="border-bottom: 1px solid black;">
          <tr>
            <td style="padding-top:10px;"></td>
            <td style="padding-top:10px;"></td>
          </tr>
        </tbody>
        @endif
        @if(!empty($portalItemCheck))
        <tbody style="border-bottom: 1px solid black;">
          <tr>
            <th align="left" style="padding-bottom: 10px;">Export auf Portale</th>
          </tr>
          @foreach ($portalItems as $item)
          <tr>
            <td align="left">{{$item->ord_item_desc}}</td>
            @if($item->ord_item_quantity_type == "pce" || $item->ord_item_quantity_type == "day")
            <?php
            $calcPrice = $item->ord_item_price * $item->ord_item_quantity;
            ?>
            <td align="right">{{number_format($calcPrice, 2, '.', "'")}}</td>
            @elseif($item->ord_item_quantity_type == "min")
            <?php 
            $calcPrice = $item->ord_item_price / 60 * $item->ord_item_quantity;
            ?>
            <td align="right">{{number_format($calcPrice, 2, '.', "'")}}</td>
            @endif
          </tr>
          @endforeach
        </tbody>
        <tbody>
          <tr>
            <td style="padding-bottom:10px;"></td>
            <td style="padding-bottom:10px;"></td>
          </tr>
        </tbody>
        <tbody>
          <tr>
            <td align="left">Subtotal</td>
            <td align="right">{{number_format($specPriceTot, 2, '.', "'")}}</td>
          </tr>
        </tbody>
        <tbody style="border-bottom: 1px solid black;">
          <tr>
            <td style="padding-top:10px;"></td>
            <td style="padding-top:10px;"></td>
          </tr>
        </tbody>
        @endif
        {{-- Start Portal details --}}
        <tbody>
          <tr>
            <td style="padding-top:20px;"></td>
            <td style="padding-top:20px;"></td>
          </tr>
        </tbody>
        <tbody style="border-bottom: 1px solid black;">
          <tr>
            <td style="padding-bottom:10px;padding-top:10px;">
              Bezeichnung
            </td>
            <td style="padding-bottom:10px;padding-top:10px;" align="right">
              CHF
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    @endif
  </main>
</body>

</html>